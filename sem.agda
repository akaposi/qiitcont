{-

We inductively define a type of algebras for IITs together with its semantics 

-}

open import Data.Product
open import Function
open import Relation.Binary.PropositionalEquality
open ≡-Reasoning

record Fam : Set₁ where
  constructor _,_
  field
    U : Set
    El : U → Set

record HomFam (X Y : Fam) : Set where
  constructor _,_
  open Fam
  field
    Uf : U X → U Y
    Elf : {x : U X} → El X x → El Y (Uf x)

_∘Hf_ : {X Y Z : Fam} → HomFam Y Z → HomFam X Y → HomFam X Z
(Uf , Elf) ∘Hf (Ug , Elg) = Uf ∘ Ug , λ y → Elf (Elg y)

data QIIT : Set₁

variable A : QIIT

Obj : QIIT → Set₁

-- container QIIT → Set
record Cont (A : QIIT) : Set₁ where
  pattern
  constructor _◁_
  field
    S : Set
    P : S → Obj A

variable C : Cont A

Hom : (X Y : Obj A) → Set
_∘A_ : {X Y Z : Obj A} → Hom Y Z → Hom X Y → Hom X Z
assoc-A : {W X Y Z : Obj A}{f : Hom Y Z}{g : Hom X Y}{h : Hom W X} → (f ∘A g) ∘A h ≡ f ∘A (g ∘A h)

-- semantics of containers on objects
{-# NO_POSITIVITY_CHECK #-} -- can be fixed
record ⟦_⟧C {A : QIIT}(C : Cont A)(X : Obj A) : Set where
  inductive
  pattern
  constructor _,_
  open Cont C
  field
    s : S
    f : Hom (P s) X

{-
⟦_⟧C : (C : Cont A)(X : Obj A) → Set
⟦ S ◁ P ⟧C X = Σ[ s ∈ S ]  Hom (P s) X
-}


-- and on homs
⟦_⟧C-hom : (C : Cont A){X Y : Obj A}(f : Hom X Y)(x : ⟦ C ⟧C X) → ⟦ C ⟧C Y
⟦ S ◁ P ⟧C-hom f (s , g) = s , (f ∘A g)

⟦_⟧C-hom-eq : (C : Cont A){X Y Z : Obj A}(f : Hom Y Z)(g : Hom X Y)(x : ⟦ C ⟧C X)
  →  ⟦ C ⟧C-hom (f ∘A g) x ≡ ⟦ C ⟧C-hom f (⟦ C ⟧C-hom g x)  
⟦ S ◁ P ⟧C-hom-eq f g (s , h)  = cong (λ k → s , k) assoc-A

record RHS (L : Cont A) : Set₁ where
  open Cont L
  field
    Q : S → Obj A
    g : (s : S) → Hom (P s) (Q s)

-- extension of a QIIT by a constructor
record Constr (A : QIIT) : Set₁ where
  constructor _,_
  pattern
  field
    -- L = S <| P : A → Set
    L : Cont A
    -- R = S <| Q : ∫ L → Set with Q_t = d
    R : RHS L

record ⟦_⟧R {L : Cont A}(R : RHS L){X : Obj A}(sf : ⟦ L ⟧C X) : Set where
  open Cont L
  open RHS R
  open ⟦_⟧C sf
  field
    h : Hom (Q s) X
    eq : h ∘A (g s) ≡ f
    
record Ext {A : QIIT}(C : Constr A) : Set₁ where
  inductive
  constructor _,_
  pattern
  open Constr C
  field
    X : Obj A
    c : (x : ⟦ L ⟧C X) → ⟦ R ⟧R x

data QIIT where
  ε : QIIT
  _,_ : (A : QIIT) → Constr A → QIIT

Obj ε = Fam
Obj (A , C) = Ext C

record HomExt {C : Constr A}(Xc Yd : Ext C) : Set where
  open Constr C
  open RHS R
  open ⟦_⟧R
  open Ext Xc
  open Ext Yd renaming (X to Y ; c to d) 
  field
    k : Hom X Y
    eq : (sf : ⟦ L ⟧C X) → let open ⟦_⟧C sf
                           in k ∘A (h (c (s , f))) ≡ h (d (s , k ∘A f)) 


Hom {A = ε} X Y = HomFam X Y
Hom {A = A , C} Xc Yd = HomExt Xc Yd 

{-
_∘A_ {A = ε} f g = f ∘Hf g
_∘A_ {A = A , (L , Q) k} {X = X , c} {Y = Y , d} {Z = Z , e} (f , fe) (g , ge) =
  f ∘A g , λ x → begin (f ∘A g) ∘A proj₁ (c x) ≡⟨ assoc-A ⟩
                       f ∘A (g ∘A proj₁ (c x)) ≡⟨ cong (λ y → f ∘A y) (ge x) ⟩
                       f ∘A (proj₁ (d (⟦ L ⟧C-hom g x))) ≡⟨ fe (⟦ L ⟧C-hom g x) ⟩
                       proj₁ (e (⟦ L ⟧C-hom f (⟦ L ⟧C-hom g x))) ≡⟨ cong (λ y → proj₁ (e y)) (sym (⟦ L ⟧C-hom-eq f g x)) ⟩                       
                       proj₁ (e (⟦ L ⟧C-hom (f ∘A g) x)) ∎
{-
Hom {A = ε} X Y = HomFam X Y
Hom {A = A , (L , R)} (X , c) (Y , d) = Σ[ f ∈ Hom X Y ] ((x : ⟦ L ⟧C X) → (∫[ L ]hom f x ∘∫ c x) ≡ d (⟦ L ⟧C-hom f x)) 
--Σ[ f ∈ Hom X Y ] ((x : ⟦ L ⟧C X) → R-lift f x (c x) ≡ d (⟦ L ⟧C-hom f x))

_∘A_ {A = ε} f g = f ∘Hf g
_∘A_ {A = A , (L , R)} {X = X , c} {Y = Y , d} {Z = Z , e} (f , fe) (g , ge) =
  (f ∘A g) , λ x → (∫[ L ]hom (f ∘A g) x ∘∫ c x) ≡⟨ {!!} ⟩
                   {! !} ≡⟨ {!!} ⟩
                   subst (λ x₁ → ∫-hom R (Z , x₁)) (sym (⟦ L ⟧C-hom-eq f g x))
                         (∫[ L ]hom f (⟦ L ⟧C-hom g x) ∘∫ (∫[ L ]hom g x ∘∫ c x))
                           ≡⟨ cong (λ z → subst (λ x₁ → ∫-hom R (Z , x₁)) (sym (⟦ L ⟧C-hom-eq f g x))
                                   (∫[ L ]hom f (⟦ L ⟧C-hom g x) ∘∫ z)) (ge x) ⟩
                   subst (λ x₁ → ∫-hom R (Z , x₁)) (sym (⟦ L ⟧C-hom-eq f g x))
                         (∫[ L ]hom f (⟦ L ⟧C-hom g x) ∘∫ d (⟦ L ⟧C-hom g x))
                     ≡⟨ cong (subst (λ x₁ → ∫-hom R (Z , x₁)) (sym (⟦ L ⟧C-hom-eq f g x))) (fe (⟦ L ⟧C-hom g x)) ⟩
                   subst (λ x → ∫-hom R (Z , x)) (sym (⟦ L ⟧C-hom-eq f g x)) (e (⟦ L ⟧C-hom f (⟦ L ⟧C-hom g x)))
                     ≡⟨ dcong (λ x → ∫-hom R (Z , x)) e (sym (⟦ L ⟧C-hom-eq f g x)) ⟩ -- e (⟦ L ⟧C-hom f ((⟦ L ⟧C-hom g x))
                   e (⟦ L ⟧C-hom (f ∘A g) x)  ∎
-}
{-
_∘A_ {A = A , (L , R)} {X = X , c} {Y = Y , d} {Z = Z , e} (f , fe) (g , ge) = (f ∘A g) ,
        λ x → begin (∫[ L ]hom (f ∘A g) x ∘∫ c x) ≡⟨ {!!} ⟩
--                     (∫[ L ]hom f (∫[ L ]hom g x)) ∘∫ c x  ≡⟨ {!!} ⟩
                     e (⟦ L ⟧C-hom (f ∘A g) x) ∎
-- Goal: (∫[ L ]hom (f ∘A g) x ∘∫ c x) ≡ e (⟦ L ⟧C-hom (f ∘A g) x)
-}
assoc-A = {!!}
-}


{-
Dependent container morphisms DCont(L,R)

L = S ◁ P : A → Set
with S : Set
     P : S → A
R = T ◁ Q : ∫ A → Set
with T : Set
     Q : T → ∫ L
             Σ X : Set . L X
             Σ X : Set, s : S, P s →_A X
     Q_X : T → A
     Q_s : T → S
     Q_g : (t : T) → P (Q_s t) →_A Q_X t

∫_(X : A) → (x : L X) → R (X , x)
= ∫_(X : A) → ((s,f) : Σ : S, P s →_A X) → Σ t : T , ∫ L (Q t , (X , s , f))
                                           Σ t : T , h : Q_X t → X , Q_s t = s, h ∘ Q_g t = f
= (s : S) → ∫_(X : A) → (f : P s →_A X) → Σ t : T , h : Q_X t → X , Q_s t = s, h ∘ Q_g t = f 
= (s : S) → Σ t : T , h : Q_X t → P s , Q_s t = s, h ∘ Q_g t = id
= Σ t : S → T , h : (s : S) → Q_X (t s) → P s, Q_s (t s) = s, h s ∘ Q_g (t s) = id


α : ∫_(X : A) → (x : L X) → R (X , x)
α X (s , f) = (t s, f ∘ h s, - , - )


for a constructor we have

L : A → Set
L = S ◁ P
R : ∫ L → Set 
with R(1,x) = 1 for all x : L 1
i.e. R = S ◁ (Q_X s, id , Q_g)
with Q_X : S → A, Q_g : (s : S) → P s →_A Q_X s

c : ∫_(X : A) → (x : L X) → R (X , x)
= Σ h : (s : S) → Q_X s → P s, h s ∘ g s = id

however, constructors for an arbitrary algebra are not natural in X. 

R (X , (s,f)) = ∫ L ((Q_X s, s, Q_g) , (X, s , f)
              = Σ h : A( Q_X s , X) , h ∘ Q_g s = f

-}

{-
We define an interpretation of the Syntax.

⟦ TT ⟧ = QIIT 
ε : TT
_+_ : (A : TT) → Constr A → TT
⟦ ε ⟧ = Fam
⟦ A + C ⟧ = Ext {A} C

we have a functor (omitting Obj)

  U : Ext {A} C →  A

the free extension is a left adjoint

  F-Ext : A → Ext {A} C

⟦ Ext A B ⟧ = adjoint functors ⟦ A ⟧ ⟦ B ⟧
idE : Ext A A
⟦ idE ⟧ = Id
fstE : Ext A (B + C) → Ext A B
⟦fstE F⟧ = U ∘ F

⟦ Con A ⟧ = Cont ⟦ A ⟧
⟦ Ty Γ ⟧ = Cont (∫ ⟦ Γ ⟧)

• : Con A
_,_ : (Γ : Con A) → Ty Γ → Con A

⟦ • ⟧ = 1 ◁ 0
0 is the initial object in A which is the free algebra 
(we get this from the free extension, and initial object in Fam)

⟦ Γ ⟧ = S ◁ P
⟦ ρ ⟧ = T ◁ Q

⟦ Γ , ρ ⟧ X 
= Σ γ : Γ X , ρ (X , γ)
= Σ s : S , f : A(P s, X) , t : T, ∫ Γ (Q t,(X,s,f))
= Σ s : S , f : A(P s, X) , t : T, k : A(Q_X t, X) , Q_s t = s, k ∘ Q_g t = f
= Σ t : T , k : A(Q_X t, X) 

⟦ Ty Γ ⟧ = Cont (∫ ⟦ Γ ⟧)
ignore the special status of RTy in the moment

uu : RTy Γ
emb : RTy Γ → Ty Γ
el : Tm Γ (emb uu) → RTy Γ

uu:

Given L , H : A → Set with L = S ◁ P, R = T ◁ Q 
we define H^L : ∫ L → Set
H^L (X , x) = H X
H^L = (s,t) : S × T ◁ (P s +A Q t , s , inl)
H^L (X , s , g ) 
= Σ (s,t) : S × T . ∫L((P s +A Q t , s , inl),(X,s',g) 
= Σ s :S, t:T . f₀ : A(P s,X), f₁ : A(Q t,X), s=s' , g=f₀
= Σ t : T. A(Q t, X)
= H X

we need coproducts in A. 

Given U : A → B, F : B → A , with U ⊢ F and L : B → Set with L = S ◁ P 
we define L[U] : A → Set, with L[U] X = L(U X)
L[U] = s : S ◁ F(P s)
L[U] 
= Σ s : S . A(F (P s), X)
= Σ s : S . A(P s, U X)
= L (U X) 

U0 : Fam → Set
U0(A,B) = A
U0 = 1 ◁ (1,0)
U0 (A,B) = Fam (1,0) (A,B) = A

Given U_A : A → Fam, and F_A : Fam → A

U1 : A → Set
U1 X 
= U0 (U X)
= 1 ◁ F_A 0 

UU : ∫ Γ → Set
UU = U1^Γ
UU = s : S ◁ (P s + F_A 0, s , inl)

⟦ Tm{A} Γ σ ⟧ = DCont_⟦A⟧ ⟦ Γ ⟧ ⟦ σ ⟧


-}


