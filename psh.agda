{-# OPTIONS --cubical #-}
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Agda.Builtin.Unit
open import Cubical.Data.Empty

module psh where

record Con : Set₁ where
  constructor mk
  field
    obΓ  : Set → Set
    morΓ : {X Y : Set} → (X → Y) → obΓ X → obΓ Y
    ∘Γ   : ∀{X Y Z}{f : X → Y}{g : Y → Z}{γ} → morΓ (λ x → g (f x)) γ ≡ morΓ g (morΓ f γ)
    idΓ  : ∀{X}{γ} → morΓ {X}{X} (λ x → x) γ ≡ γ
open Con public renaming (obΓ to ob; morΓ to mor; ∘Γ to ∘; idΓ to id)

record Sub (Γ Δ : Con) : Set₁ where
  constructor mk
  field
    obσ  : {X : Set} → ob Γ X → ob Δ X
    morσ : {X Y : Set}(f : X → Y)(γ : ob Γ X) → mor Δ f (obσ γ) ≡ obσ (mor Γ f γ)
open Sub public renaming (obσ to ob; morσ to mor)

record Ty (Γ : Con) : Set₁ where
  constructor mk
  field
    obA  : {X : Set} → ob Γ X → Set
    morA : {X Y : Set}(f : X → Y){γ : ob Γ X} → obA γ → obA (mor Γ f γ)
    ∘A   : ∀{X Y Z}{f : X → Y}{g : Y → Z}{γ : ob Γ X}{α : obA γ} → subst obA (∘ Γ) (morA (λ x → g (f x)) α) ≡ morA g (morA f α)
    idA  : ∀{X}{γ}{α : obA γ} → subst obA (id Γ) (morA {X}{X} (λ x → x) α) ≡ α
open Ty public renaming (obA to ob; morA to mor; ∘A to ∘; idA to id)
