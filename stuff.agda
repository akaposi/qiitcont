{-# OPTIONS --cubical --no-forcing #-}
open import Cubical.Core.Everything 
open import Cubical.Foundations.Prelude hiding (Σ-syntax)
open import Data.Nat
open import Relation.Nullary
open import Data.Empty
open import Data.Unit
open import Data.Product

{-
data A : Type where
  a b c : A
  p₀ : a ≡ b
  p₁ : b ≡ c
  p₂ : a ≡ c
  α : p₀ ∙ p₁ ≡ p₂
-}

{-
data A' : Type where
  a b c : A'
  p₀ : a ≡ b
  p₁ : b ≡ c
  p₂ : a ≡ c
  p₀p₁ : a ≡ c
  filler : PathP (λ i → a ≡ p₁ i) p₀ p₀p₁
  α : p₀p₁ ≡ p₂


data A' : Type where
  a : A'
  p₀ : a ≡ a
  p₁ : a ≡ a
  p₂ : a ≡ a
  filler : PathP (λ i → a ≡ p₁ i) p₀ p₂

data A'' : Type where
  a b c : A''
  p₀ : a ≡ b
  p₁ : b ≡ c
  p₂ : a ≡ c
  α : PathP (λ i → a ≡ p₁ i) p₀ p₂
-}

variable
    ℓ ℓ' : Level
    A B : Type 
    C : A → Type 
    x y z w : A
    
subst' : (B : A → Type)(p : x ≡ y) → B x → B y
subst' B p b = transp (λ i → B (p i)) i0 b

subst≡ : (B : A → Type)(p : x ≡ y)(b : B x) → PathP (λ i → B (p i)) b (subst B p b)
subst≡ B p b i = transp (λ j → B (p (i ∧ j))) (~ i) b 
{-
Goal: B (p i)
———— Boundary ——————————————————————————————————————————————
i = i0 ⊢ b
i = i1 ⊢ subst B p b

transp (λ j → B (p (i ∧ j))) (~ i) b 

i = i0 :
transp (λ j → B (p (i0 ∧ j))) (~ i0) b 
= transp (λ j → B i0) i1 n
= b

i = i1:
transp (λ j → B (p (i1 ∧ j))) (~ i1) b
= transp (λ j → B (p j)) i0 b
= subst B p b
-}


isZero : ℕ → Type
isZero zero = ⊤
isZero (suc n) = ⊥

cons : {n : ℕ} → ¬ (0 ≡ suc n)
cons p = transp (λ i → isZero (p i)) i0 tt
--cons p = subst isZero p _

isContr⊤ : isContr ⊤
isContr⊤ = _ , (λ y i → y)

isProp⊥ : isProp ⊥
isProp⊥ () y



×-eq : {ab ab' : A × B} → ab ≡ ab' → (proj₁ ab ≡ proj₁ ab') × (proj₂ ab ≡ proj₂ ab')
proj₁ (×-eq p) i = proj₁ (p i)
proj₂ (×-eq p) i = proj₂ (p i)

×-eq' : {ab ab' : A × B} → (proj₁ ab ≡ proj₁ ab') × (proj₂ ab ≡ proj₂ ab')  → ab ≡ ab'
proj₁ (×-eq' pq i) = proj₁ pq i
proj₂ (×-eq' pq i) = proj₂ pq i

Σ-eq : {ab ab' : Σ A C} → ab ≡ ab' → Σ (proj₁ ab ≡ proj₁ ab') (λ p → PathP (λ i → C (p i)) (proj₂ ab) (proj₂ ab'))
proj₁ (Σ-eq p) i = proj₁ (p i)
proj₂ (Σ-eq p) i = proj₂ (p i)

Σ-eq' : {ab ab' : Σ A C} → Σ (proj₁ ab ≡ proj₁ ab') (λ p → PathP (λ i → C (p i)) (proj₂ ab) (proj₂ ab'))  → ab ≡ ab'
proj₁ (Σ-eq' pq i) = proj₁ pq i
proj₂ (Σ-eq' pq i) = proj₂ pq i

{-
isProp : Type ℓ → Type ℓ
isProp A = (x y : A) → x ≡ y
-}

isProp× : isProp A → isProp B → isProp (A × B)
isProp× f g (a , b) (a' , b') i = (f a a' i) , (g b b' i)

{-
isPropΣ : isProp A → ((a : A) → isProp (C a)) → isProp (Σ A C)
--isPropΣ {C = C} f g (a , c) (a' , c') i = (f a a' i) , {!!}
-}

{-
isPropΣ : isProp A → ((a : A) → isProp (C a)) → isProp (Σ A C)
isPropΣ {C = C} f g (a , c) (a' , c') i = p i , toPathP {A = λ i → C (p i)} q i
  where p = f a a'
        q : subst C p c ≡ c'
        q = g a' (subst C p c) c'
-}

isPropΣ : isProp A → ((a : A) → isProp (C a)) → isProp (Σ A C)
{-
isPropΣ {C = C} f g (a , c) (a' , c') i =
  p i , g (p i) (transp (λ j → C (p (i ∧ j))) (~ i) c) (transp (λ j → C (p (i ∨ ~ j))) i c') i
  where
  p = f a a'
-}
variable i j k : I

coe-0 : (A : I → Type) → A i0 → A i
coe-0 {i} A x = transp (λ j → A (i ∧ j)) (~ i) x


-- 


coe-1 : (A : I → Type) → A i1 → A i
coe-1 {i} A x = transp (λ j → A (i ∨ ~ j)) i x

isPropΣ {C = C} f g (a , c) (a' , c') i =
  p i , g (p i) (coe-0 {i} (λ k → C (p k)) c) (coe-1 {i} (λ k → C (p k)) c') i
  where
  p = f a a'

{-
coe-ij : (A : I → Type) → A i → A j
coe-ij {i = i}{j = j} A x = transp (λ k → A ((~ k ∧ i) ∨ (k ∧ j))) (i ∧ j) x
-}


{-

{-
isPropΣ : isProp A → ((a : A) → isProp (C a)) → isProp (Σ A C)
proj₁ (isPropΣ {C = C} f g (a , c) (a' , c') i) = f a a' i
proj₂ (isPropΣ {C = C} f g (a , c) (a' , c') i) = {! !}
-}

-- g a' (transp (λ i → C (f a a' i)) i0 c) c' i

{-
(a , b) ≡ (a' , b')

f : (a a' : A) → a ≡ a'

p : a ≡ a'      f a a'
q : p* b ≡ b'   g a' (p* b) b'


g : (a : A)(c c' : C a) → c ≡ c'

? : (a a' : A)(c : C a)(c' : C a') → PathP (λ i → C (f a a' i)) c c')

-}



isPropΣ' : (f : isProp A) → ((a a' : A)(c : C a)(c' : C a') → PathP (λ i → C (f a a' i)) c c') → isProp (Σ A C)
isPropΣ' f g (a , c) (a' , c') i = (f a a' i) , g a a' c c' i
{-
f a a' : a ≡ a'
c : C a
c' : C a'
PathP C c c' 

-}

{-
  toPathP : transp (\ i → A i) i0 x ≡ y → PathP A x y
  toPathP p i = hcomp (λ j → λ { (i = i0) → x
                               ; (i = i1) → p j })
                      (transp (λ j → A (i ∧ j)) (~ i) x)

-}


isPropΣ : isProp A → ((a : A) → isProp (C a)) → isProp (Σ A C)
isPropΣ {C = C} f g (a , c) (a' , c') i = p i , toPathP {A = λ i → C (p i)} q i
  where p = f a a'
        q : subst C p c ≡ c'
        q = g a' (subst C p c) c'



-- Evan's version
isPropΣ : isProp A → ((a : A) → isProp (C a)) → isProp (Σ A C)
{-
isPropΣ {C = C} f g (a , c) (a' , c') i =
  p i , g (p i) (transp (λ j → C (p (i ∧ j))) (~ i) c) (transp (λ j → C (p (i ∨ ~ j))) i c') i
  where
  p = f a a'
-}

variable i j k : I

coe-0 : (A : I → Type) → A i0 → A i
coe-0 {i} A x = transp (λ j → A (i ∧ j)) (~ i) x

coe-1 : (A : I → Type) → A i1 → A i
coe-1 {i} A x = transp (λ j → A (i ∨ ~ j)) i x

isPropΣ {C = C} f g (a , c) (a' , c') i =
  p i , g (p i) (coe-0 {i} (λ k → C (p k)) c) (coe-1 {i} (λ k → C (p k)) c') i
  where
  p = f a a'


coe-ij : (A : I → Type) → A i → A j
coe-ij {i = i}{j = j} A x = transp (λ k → A ((~ k ∧ i) ∨ (k ∧ j))) (i ∧ j) x

--coe-ij {i = i}{j = j} A x = transp (λ k → A (~ (k ∨ ~ i) ∧ (~ k ∨ ~ j))) (~ i ∧ ~ j) x

-- k = i0 ==> i
-- k = i1 ==> j
-- (~ k ∧ i) ∨ (k ∧ j)
-- ~~((~ k ∧ i) ∨ (k ∧ j)) =
-- ~ (k ∨ ~ i) ∧ (~ k ∨ ~ j)


{-
isPropΣ : isProp A → ((a : A) → isProp (C a)) → isProp (Σ A C)
isPropΣ {C = C} f g = isPropΣ' f (λ a a' c c' → toPathP (g a' (subst C (f a a') c) c'))
-}
--toPathP (g a' (transp (λ i → C (f a a' i)) i0 c) c'))
{-
  toPathP : transp (\ i → A i) i0 x ≡ y → PathP A x y
  toPathP p i = hcomp (λ j → λ { (i = i0) → x
                               ; (i = i1) → p j })
                      (transp (λ j → A (i ∧ j)) (~ i) x)
-}

{-
in SetTT

((a ,c) (a , c') : Σ A C) → ac ≡ ac'
Σ p : a ≡ a' . c ≡[p] c'
p = f a
coh p : c ≡[p] subst p c ≡ c'
g (subst p c) c' : subst p c ≡ c'
trans (coh p) (g (subst p c) c') : c ≡[p;refl] c'
-}

isSet× : isSet A → isSet B → isSet (A × B)
isSet× f g (a , b) (a' , b') p q i j =
  (f a a' (λ i → proj₁ (p i)) (λ i → proj₁ (q i)) i j) ,
   g b b' (λ i → proj₂ (p i)) (λ i → proj₂ (q i)) i j

{-
isSetΣ' : isSet A → ({a a' : A}(p : a ≡ a')(i : I) → isSet (C (p i))) → isSet (Σ A C)
isSetΣ' f g (a , c) (a' , c') p q i j =
  (f a a' (λ i → proj₁ (p i)) (λ i → proj₁ (q i)) i j) ,
  {!g {a} {a'} (λ i → proj₁ (p i)) i (proj₂ (p i))   !}

isSetΣ : isSet A → ((a : A) → isSet (C a)) → isSet (Σ A C)
isSetΣ f g = isSetΣ' f (λ p i → g (p i)) 
-}
{-
isSetΣ : isSet A → ((a : A) → isSet (C a)) → isSet (Σ A C)
isSetΣ {A = A}{C = C} f g (a , c) (a' , c') p q i j =
  (f a a' (λ i → proj₁ (p i)) (λ i → proj₁ (q i)) i j ,
   {!g a' (transp (λ i → C (proj₁ (p i))) i0 c) c' (λ i → proj₂ (q i))!})
-}
{-
  (f a a' (λ i → proj₁ (p i)) (λ i → proj₁ (q i)) i j) ,
  {!g a c !}
-}
-}

_∙x_ : {A : Set}{a b c : A} → a ≡ b → b ≡ c → a ≡ c
_∙x_ {a = a} p q i = hcomp (λ {j (i = i0) → a ; j (i = i1) → q j}) (p i) 

module I where 
  data T : Set where
    a b : T
    p : a ≡ b
    q : b ≡ a
    pq : p ∙x q ≡ refl

record T-Alg : Set₁ where
  field 
    T : Set 
    a b : T
    p : a ≡ b
    q : b ≡ a
    pq : p ∙x q ≡ refl


module _ (M : T-Alg) where

  module M = T-Alg M

  rec-T : I.T → M.T
  rec-T I.a = M.a
  rec-T I.b = M.b
  rec-T (I.p i) = M.p i
  rec-T (I.q i) = M.q i
  rec-T (I.pq i j) = {!M.pq i j!}
{-
transp (λ i₁ → T-Alg.T M₁) i₁
(rec-T ((λ { i₂ (i0 = i0) → I.a }) i₁ _))
!= T-Alg.a M₁ of type T-Alg.T M₁


Goal 
rec-T I.pq : rec-T p ∙ rec-T q

Given 
M.pq : M.p ∙ M.q



i = i0 ⊢ hcomp
         (λ i₁ .o →
            transp (λ i₂ → M.T) i₁
            (rec-T ((λ { i₂ (j = i0) → I.a ; i₂ (j = i1) → I.q i₂ }) i₁ _)))
         (transp (λ i₁ → M.T) i0 (M.p j))
i = i1 ⊢ M.a
j = i0 ⊢ M.a
j = i1 ⊢ M.a
-}
