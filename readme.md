## A container model of type theory

* [Abstract](types2021/abstract.lagda) submitted to TYPES 2021
* [Formalisation](model.agda)
