{-# OPTIONS --cubical #-}
open import Agda.Builtin.Cubical.Sub hiding (Sub)
open import Agda.Builtin.Unit
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Data.Empty
open import Cubical.Data.Sigma hiding (Sub)
open import Cubical.Data.Sum renaming (elim to elim⊎)
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Univalence
open import Cubical.HITs.Pushout

module experiment where

module notes {A B : Type}{f : A → B}{g : B → A}{β : (y : B) → f (g y) ≡ y}{η : (x : A) → g (f x) ≡ x}{x : A}{y : B} where
  ex : transport (ua (isoToEquiv (iso f g β η))) x ≡ (f x)
  ex i = transp (λ _ → B) i (f x)

  ex' : transport (ua (isoToEquiv (iso f g β η))) (g y) ≡ y
  ex' i = transp (λ _ → B) i (β y i)

  ex'' : PathP (λ i → ua (isoToEquiv (iso f g β η)) i) x (f x)
  ex'' = toPathP ex

module notes1 (A : Type)(x00 x01 x10 x11 : A)(x02 : x00 ≡ x01)(x20 : x00 ≡ x10)(x21 : x01 ≡ x11) where
  x12 : x10 ≡ x11
  x12 i = hcomp (λ j → λ { (i = i0) → x20 j ; (i = i1) → x21 j }) (x02 i) 

  x22 : (i : I) → x02 i ≡ x12 i
  x22 i j = hfill (λ j → λ { (i = i0) → x20 j ; (i = i1) → x21 j }) (inc (x02 i)) j

  x22' : (j : I) → x20 j ≡ x21 j
  x22' j i = x22 i j

module notes2 (A : Type)(f : I → A) where
  -- the same square with less descriptive...
  sq : I → I → A
  sq i j = f (i ∨ j)

  -- ... and more descriptive type
  sq' : PathP (λ i → f i ≡ f i1) (λ i → f i) (λ _ → f i1)
  sq' i j = f (i ∨ j)

module notes3 (A : Set)(x y : A)(p : x ≡ y)(i j k : I) where
  square : A
  square = hcomp (λ k → λ {
     (i = i0) → p k ;
     (i = i1) → p k ;
     (j = i0) → p k ;
     (j = i1) → p k })
    x

  square' : A
  square' = hcomp (λ k → λ {
     (i = i0) → x ;
     (i = i1) → p k ;
     (j = i0) → p (i ∧ k) ;
     (j = i1) → p (i ∧ k) })
    x

  square'' : A
  square'' = hcomp (λ k → λ {
     (i = i0) → x ;
     (i = i1) → p k ;
     (j = i0) → hfill (λ k → λ { (i = i0) → x ; (i = i1) → p k }) (inc x) k ;
     (j = i1) → p (i ∧ k)})
    x

  trconst : transp (λ _ → A) i0 x ≡ x
  trconst i = transp (λ _ → A) i x

module notes4
  (A : Type)(a b c d : A)(p : a ≡ b)(q : c ≡ d)(r : a ≡ c)(s : b ≡ d)
  where
  {-
  c--q--d
  |     |
  r     s
  |     |
  a--p--b
  -}
  from : PathP (λ j → r j ≡ s j) p q → PathP (λ i → p i ≡ q i) r s
  from e i j = e j i

  to : PathP (λ i → p i ≡ q i) r s → PathP (λ j → r j ≡ s j) p q
  to e i j = e j i

  fromto : ∀ e → from (to e) ≡ e
  fromto e = refl

  tofrom : ∀ e → to (from e) ≡ e
  tofrom e = refl

module notes5 where
  comp1 : ∀{A B : Type}(p : A ≡ B){x : A}{y z : B}(e1 : PathP (λ i → p i) x y)(e2 : PathP (λ i → p i) x z) → y ≡ z
  comp1 {A}{B} p {x}{y}{z} e1 e2 i = comp (λ j → p (i ∨ j)) (λ j → λ { (i = i0) → e1 j ; (i = i1) → z }) (e2 i)

module _
  {ℓ}{A : Type ℓ}{ℓ'}{B : A → Type ℓ'}{ℓ''}{C : A → Type ℓ''}(f : (a : A) → B a → C a){a₀ a₁ : A}(p : a₀ ≡ a₁)(b : B a₀)
  where

  transp-$₀ : PathP (λ i → C (p i)) (f a₀ b) (f a₁ (transp (λ j → B (p j)) i0 b))
  transp-$₀ i = f (p i) (transp (λ j → B (p (i ∧ j))) (~ i) b)
  
  transp-$₁ : PathP (λ i → C (p i)) (f a₀ b) (transp (λ j → C (p j)) i0 (f a₀ b))
  transp-$₁ i = transp (λ j → C (p (i ∧ j))) (~ i ∨ i0) (f a₀ b)

  transp-$ : f a₁ (transp (λ j → B (p j)) i0 b) ≡ transp (λ j → C (p j)) i0 (f a₀ b)
  transp-$ i = comp (λ j → C (p (i ∨ j))) (λ j → λ { (i = i0) → transp-$₀ j ; (i = i1) → transp (λ j → C (p j)) i0 (f a₀ b) }) (transp-$₁ i)

module elim where

  module I where
    data T² : Set where
      b : T²
      p q : b ≡ b
      t : p ∙ q ≡ q ∙ p

  record alg : Set₁ where
    field
      T²  : Set
      b   : T²
      p q : b ≡ b
      t   : p ∙ q ≡ q ∙ p

  module _ (M : alg) where
    module M = alg M
    {-
    ⟦_⟧ : I.T² → M.T²
    ⟦ I.b ⟧ = M.b
    ⟦ I.p i ⟧ = M.p i
    ⟦ I.q i ⟧ = M.q i
    ⟦ I.t i j ⟧ = {!M.t i j!}
    -}

module elim1 where

  module I where
    data A : Set
    data B : A → Set
    data A where
      a  : A
      ea : a ≡ a
      a' : B a → A
    data B where
      b  : B a
      eb : transp (λ i → B (ea i)) i0 b ≡ b

  record alg : Set₁ where
    field
      A  : Set
      B  : A → Set
      a  : A
      ea : a ≡ a
      a' : B a → A
      b  : B a
      eb : transp (λ i → B (ea i)) i0 b ≡ b

  module _ (M : alg) where
    module M = alg M
{-
    ⟦_⟧A : I.A → M.A
    ⟦_⟧B : {a : I.A} → I.B a → M.B ⟦ a ⟧A

    ⟦ I.a ⟧A = M.a
    ⟦ I.ea i ⟧A = M.ea i
    ⟦ I.a' y ⟧A = M.a' ⟦ y ⟧B
    ⟦ I.b ⟧B = M.b
    ⟦ I.eb i ⟧B = (transp-$ (λ x → ⟦_⟧B {x}) I.ea I.b ∙ M.eb) i
-}

module square
  (A00 A01 A10 A11 : Type)
  (A02 : A00 ≡ A01)
  (A12 : A10 ≡ A11)
  (A20 : A00 ≡ A10)
  (A21 : A01 ≡ A11)
  (A22 : PathP (λ i → A20 i ≡ A21 i) A02 A12)
  (x00 : A00)
  (x01 : A01)
  (x10 : A10)
  (x11 : A11)
  (x02 : PathP (λ j → A02 j) x00 x01)
  (x12 : PathP (λ j → A12 j) x10 x11)
  (x20 : PathP (λ i → A20 i) x00 x10)
  (x21 : PathP (λ i → A21 i) x01 x11)
  (x22 : PathP (λ i → PathP (λ j → A22 i j) (x20 i) (x21 i)) x02 x12)
  where
{-      21
     01----11
   02|      |12
     00----10
        20          -}

module transf
  (A : Type)
  (a b : A)
  (p q : a ≡ b)
  (X : p ≡ q)
  where
  {-
    b===b           b===b
   p| X |q   -->   p| Y ∥b
    a===a           a---b
                      q
  -}
--   Y : PathP (λ i → q i ≡ b) p refl
--  Y i j = {!X ? (i ∨ j)!}

  -- Y : PathP (λ i → q i ≡ b) p refl
  -- Y i j = {!X (i ∧ ~ j ∨ )!}
{-
  if ~ i then 0 else ~ j
  ~ ~ i ∧ ~ j

  if i then ~ j else 0
  i ∧ ~ j
  
  

  if p then q else r
  p ∧ q ∨ ~ p ∧ r

  X i0 j = p j
  X i1 j = q j

  Y i0 j = p j
  Y i i0 = q i
  -}

module txa
  (X Y Z : Type)
  (A : X ≡ Y)
  (B : Y ≡ Z)
  (a : X)(b : Y)(c : Z)
  (p : PathP (λ i → A i) a b)
  (q : PathP (λ i → B i) b c)
  where
  C : I → Type
  C i = (A ∙ B) i
  D : I → I → Type
  D i j = hfill (λ {k (i = i0) → X ; k (i = i1) → B k}) (inS (A i)) j
  -- D i0 i0 = X
  -- D i i0 = A i
  -- D i1 i0 = Y
  -- D i1 j = B j
  -- D i1 i1 = Z

  pq : PathP (λ i → D i i1) a c
  pq i = comp (λ j → D i j) (λ {j (i = i0) → a ; j (i = i1) → q j}) (p i)

{-
p : PathP A a b
q : PathP B b c
-------------------
p * q : Path C a c
a : A x
b : A y
c : A z

xy : x = y
yz : y = z

A B : I → Type
A ~ A i0 = A i1


A i1 = B i0

C i = hcomp 





trans (sym q) q = refl
-}




module first where
  module I where
    data T : Type where
      a b : T
      p : a ≡ b
      q : b ≡ a
      e : p ∙ q ≡ refl

  record Alg : Type₁ where
    field
      T : Type
      a b : T
      p : a ≡ b
      q : b ≡ a
      e : p ∙ q ≡ refl

  module recursor (M : Alg) where
    module M = Alg M
    {-
    recT : I.T → M.T
    recT I.a = M.a
    recT I.b = M.b
    recT (I.p i) = M.p i
    recT (I.q i) = M.q i
    recT (I.e i j) = {!M.e i j!} -- this can be filled if you turn off the termination checker
-}

module second where
  module I where
    data T : Type where
      a b : T
      p   : a ≡ b
      q   : b ≡ a
      pq  : a ≡ a
      pq= : PathP (λ i → p i ≡ a) pq q
      {-
         a ===> a
         ^      ^
       pq|      | q
         a ---> b
            p
      -}
      e : pq ≡ refl

  record Alg : Type₁ where
    field
      T : Type
      a b : T
      p : a ≡ b
      q : b ≡ a
      pq  : a ≡ a
      pq= : PathP (λ i → p i ≡ a) pq q
      e : pq ≡ refl

  module recursor (M : Alg) where
    module M = Alg M
    recT : I.T → M.T
    recT I.a = M.a
    recT I.b = M.b
    recT (I.p i) = M.p i
    recT (I.q i) = M.q i
    recT (I.pq i) = M.pq i
    recT (I.pq= i i₁) = M.pq= i i₁
    recT (I.e i i₁) = M.e i i₁

module torusFirst where
  module I where
    data T : Type where
      a   : T
      p q : a ≡ a
      e   : p ∙ q ≡ q ∙ p

  -- EX: derive the recursor

module torusSecond where
  module I where
    data T : Type where
      a   : T
      p q : a ≡ a
      r   : a ≡ a
      {-                     q       =        =
        r| |q   r| |p       p p    p| |q    p| ∥
          -       -          q       =        -
          p       q                           q
      -}
      pq  : PathP (λ i → p i ≡ a) r q
      qp  : PathP (λ i → q i ≡ a) r p

  -- EX: show that the two are equivalent

module transpExample (A : Set)(a : A)(ea : a ≡ a) where
  module I where
    data B : A → Set where
      b  : B a
      eb : transp (λ i → B (ea i)) i0 b ≡ b

  record alg : Set₁ where
    field
      B  : A → Set
      b  : B a
      eb : transp (λ i → B (ea i)) i0 b ≡ b

  module _ (M : alg) where
    module M = alg M
    {-
    recB : {a : A} → I.B a → M.B a
    recB I.b = M.b
    recB (I.eb i) = {! M.eb  !}
    -}

module encDec (A : Type) where
  open import Data.List

  data List= : List A → List A → Type where
    []=  : List= [] []
    _∷=_ : {a a' : A} → a ≡ a' → {as as' : List A} → List= as as' → List= (a ∷ as) (a' ∷ as')

  reflList : ∀{xs} → List= xs xs
  reflList {[]} = []=
  reflList {x ∷ xs} = refl ∷= reflList {xs}

  enc : ∀{xs ys} → xs ≡ ys → List= xs ys
  enc {xs}{ys} = J (λ ys _ → List= xs ys) (reflList {xs})

  encRefl : ∀{xs} → enc (refl {x = xs}) ≡ reflList
  encRefl {xs} = JRefl (λ ys _ → List= xs ys) (reflList {xs})

  enc∷ : {x y : A}{xs ys : List A}(e : x ≡ y)(es : xs ≡ ys) →
    enc (λ i → e i ∷ es i) ≡ (e ∷= enc es)
  enc∷ {x}{y}{xs}{ys} e es = J
    (λ y e → enc (λ i → e i ∷ es i) ≡ (e ∷= enc es))
    (J
      (λ ys es → enc (λ i → x ∷ es i) ≡ (refl {x = x} ∷= enc es))
      (encRefl {x ∷ xs} ∙ cong ((λ _ → x) ∷=_) (sym (encRefl {xs})))
      es)
    e

  dec : ∀{xs ys} → List= xs ys → xs ≡ ys
  dec []= = refl
  dec (e ∷= es) i = e i ∷ dec es i

  decRefl : ∀{xs} → dec (reflList {xs}) ≡ refl
  decRefl {[]} = refl
  decRefl {x ∷ xs} i j = x ∷ decRefl {xs} i j

  decenc : ∀{xs ys}(e : xs ≡ ys) → dec (enc e) ≡ e
  decenc {xs}{ys} = J (λ ys e → dec (enc e) ≡ e) ((λ i → dec (encRefl {xs} i)) ∙ decRefl {xs})

  encdec : ∀{xs ys}(e : List= xs ys) → enc (dec e) ≡ e
  encdec []= = encRefl {[]}
  encdec (e ∷= es) = enc∷ e (dec es) ∙ sym (cong (e ∷=_) (sym (encdec es)))

  fruits : ∀{xs ys} → (xs ≡ ys) ≡ List= xs ys
  fruits {xs}{ys} = ua (isoToEquiv (iso enc dec encdec decenc))

module someComp
  (A B C : Type)
  (a : A)(b : B)(c : C)
  (P : A ≡ B)(Q : B ≡ C)
  (p : PathP (λ i → P i) a b)(q : PathP (λ i → Q i) b c)
  (E : A ≡ C)
  where
  {-
      ac      AC
     a->c    A->C
     ^  ^    ^  ^
     ∥  |q   ∥  |Q
     a->b    A->B
      p       P

            SQUARE
  -}

  AC : A ≡ C
  AC i = hcomp (λ j → λ { (i = i0) → A ; (i = i1) → Q j }) (P i)
  SQUARE : PathP (λ i → P i ≡ AC i) (λ j → A) Q
  SQUARE i j = hfill (λ j → λ { (i = i0) → A ; (i = i1) → Q j }) (inS (P i)) j
  ac : PathP (λ i → AC i) a c
  ac i = comp (λ j → SQUARE i j) (λ j → λ { (i = i0) → a ; (i = i1) → q j }) (p i)

  CC : C ≡ C
  CC j = hcomp (λ i → λ { (j = i0) → AC i ; (j = i1) → E i}) A
  newSQUARE' : PathP (λ j → A ≡ CC j) AC E
  newSQUARE' j i = hfill (λ i → λ { (j = i0) → AC i ; (j = i1) → E i}) (inS A) i
  newSQUARE : PathP (λ i → AC i ≡ E i) (λ j → A) CC
  newSQUARE i j = newSQUARE' j i
  acE : PathP (λ i → E i) a c -- <- this is probably impossible
  acE i = {!fill (λ j → newSQUARE i j) ()!}
