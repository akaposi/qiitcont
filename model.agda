{-# OPTIONS --cubical #-}
open import Agda.Builtin.Cubical.Sub hiding (Sub)
open import Agda.Builtin.Unit
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Data.Empty
open import Cubical.Data.Sigma hiding (Sub)
open import Cubical.Data.Sum renaming (elim to elim⊎)
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Univalence
open import Cubical.HITs.Pushout

import psh as P

-- types are also containers
-- TODO: we should use hSets

infixl 5 _▹_
infixl 7 _[_]T
infixl 5 _,s_
infixr 6 _∘_
infixl 8 _[_]t

record Con : Set₁ where
  constructor mk
  field
    SΓ : Set
    PΓ : SΓ → Set
open Con renaming (SΓ to S; PΓ to P)

record Sub (Γ Δ : Con) : Set where
  constructor mk
  field
    Sσ : S Γ → S Δ
    Pσ : {sΓ : S Γ} → P Δ (Sσ sΓ) → P Γ sΓ
open Sub renaming (Sσ to S; Pσ to P)

Sub= : ∀{Γ Δ}{σ δ : Sub Γ Δ}(S= : S σ ≡ S δ)(P= : PathP (λ i → {sΓ : S Γ} → P Δ (S= i sΓ) → P Γ sΓ) (P σ) (P δ)) →
  σ ≡ δ
Sub= S= P= i = mk (S= i) (P= i)

_∘_ : ∀{Γ Θ Δ} → Sub Θ Δ → Sub Γ Θ → Sub Γ Δ
S (σ ∘ δ) = λ sΓ → S σ (S δ sΓ)
P (σ ∘ δ) = λ pΔ → P δ (P σ pΔ)

id : ∀{Γ} → Sub Γ Γ
S id = λ sΓ → sΓ
P id = λ pΓ → pΓ

· : Con
S · = ⊤
P · * = ⊥

ε : ∀{Γ} → Sub Γ ·
S ε _ = tt
P ε ()

record Ty (Γ : Con) : Set₁ where
  constructor mk
  field
    SA : S Γ → Set
    PA : {sΓ : S Γ} → SA sΓ → Set
    QA : {sΓ : S Γ}(sA : SA sΓ) → P Γ sΓ → PA sA
open Ty renaming (SA to S; PA to P; QA to Q)

Ty= : {Γ : Con}{A B : Ty Γ}
  (S= : S A ≡ S B)
  (P= : PathP (λ i → {sΓ : S Γ} → S= i sΓ → Set) (P A) (P B)) →
  (Q= : PathP (λ i → {sΓ : S Γ}(sA : S= i sΓ) → P Γ sΓ → P= i sA) (Q A) (Q B)) →
  A ≡ B
Ty= S= P= Q= i = mk (S= i) (P= i) (Q= i)

_[_]T : ∀{Δ}(A : Ty Δ){Γ}(σ : Sub Γ Δ) → Ty Γ
S (A [ σ ]T) sΓ      = S A (S σ sΓ)
P (A [ σ ]T) {sΓ} sA = Pushout (P σ {sΓ}) (Q A sA)
Q (A [ σ ]T) {sΓ} sA pΓ = inl pΓ
{-
S A : S Δ → Type
P A : {sΔ : S Δ} → S A sΔ → Set
Q A : {sΔ : Con.S Δ}(sA : S A sΔ) → P Δ sΔ → P A sA

S σ : S Γ → S Δ
P σ : {sΓ : Con.S Γ} → P Δ (S σ sΓ) → P Γ sΓ

-- other presentation:
S' A : Type
PX A : S' A → Set
Ps A : S' A → S Δ
Pd A : (sA : S' A) → P Δ (Ps A sA) → PX A sA

S' A           := (sΔ : S Δ) × S A sΔ
PX A (sΔ , sA) := P A sA
Ps A (sΔ , sA) := sΔ
Pd A (sΔ , sA) pΔ := Q A sA pΔ

pullback: S'(A[σ]) := (sA : S' A) × (sΓ : S Γ) × (Ps A sA = S σ sΓ) ≅ (sΓ : S Γ)×(sA : S A (S σ sΓ))

S'(A[σ])----> S' A
 | _|          |
 |             |Ps A
 v             v
S Γ --------> S Δ
       S σ

pushout:
             P σ {sΓ}                         P σ {sΓ}         
 P Δ (S σ sΓ) ------> P Γ sΓ      P Δ (S σ sΓ) ------> P Γ sΓ  
       |                                |                      
Pd A sA|                          Q A sA|                      
       v                                v                      
    PX A sA                          P A sA                   
-}

record Tm (Γ : Con)(A : Ty Γ) : Set where
  constructor mk
  field
    St : (sΓ : S Γ) → S A sΓ
    Pt : {sΓ : S Γ} → P A (St sΓ) → P Γ sΓ
    Qt : {sΓ : S Γ} → _≡_ {A = P Γ sΓ → P Γ sΓ} (λ pΓ → pΓ) (λ pΓ → Pt (Q A (St sΓ) pΓ))
open Tm renaming (St to S; Pt to P; Qt to Q)
{-             P t
    P Γ sΓ <---------- P A (S t sΓ)
           ---------->
           Q A (S t sΓ)                -}

Tm= : {Γ : Con}{A₀ A₁ : Ty Γ}(A= : A₀ ≡ A₁)
  {u₀ : Tm Γ A₀}{u₁ : Tm Γ A₁}
  (S= : PathP (λ i → (sΓ : S Γ) → S (A= i) sΓ) (S u₀) (S u₁))
  (P= : PathP (λ i → {sΓ : S Γ} → P (A= i) (S= i sΓ) → P Γ sΓ) (P u₀) (P u₁)) →
  (Q= : PathP (λ i → {sΓ : S Γ} → _≡_ {A = P Γ sΓ → P Γ sΓ} (λ pΓ → pΓ) (λ pΓ → P= i (Q (A= i) (S= i sΓ) pΓ))) (Q u₀) (Q u₁)) →
  PathP (λ i → Tm Γ (A= i)) u₀ u₁
Tm= A= S= P= Q= i = mk (S= i) (P= i) (Q= i)

_[_]t : ∀{Δ}{A : Ty Δ}(t : Tm Δ A){Γ}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
S (t [ σ ]t) = λ sΓ → S t (S σ sΓ)
P (t [ σ ]t) (inl pΓ) = pΓ
P (t [ σ ]t) (inr pA) = P σ (P t pA)
P (t [ σ ]t) (push pΔ i) = cong (λ z → P σ (z pΔ)) (Q t) i
Q (t [ σ ]t) = refl

_▹_ : (Γ : Con) → Ty Γ → Con
S (Γ ▹ A) = Σ (S Γ) (S A)
P (Γ ▹ A) (sΓ , sA) = P A sA

p : ∀{Γ A} → Sub (Γ ▹ A) Γ
S (p {Γ} {A}) (sΓ , sA)    = sΓ
P (p {Γ} {A}) {sΓ , sA} pΓ = Q A sA pΓ

q : ∀{Γ A} → Tm (Γ ▹ A) (A [ p {Γ}{A} ]T)
S (q {Γ} {A}) (sΓ , sA) = sA
P (q {Γ} {A}) {sΓ , sA}(inl pA) = pA
P (q {Γ} {A}) {sΓ , sA}(inr pΓ) = pΓ
P (q {Γ} {A}) {sΓ , sA}(push pΓ i) = Q A sA pΓ
Q (q {Γ} {A}) = refl

_,s_ : ∀{Γ Δ A}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▹ A)
S (σ ,s t) sΓ = S σ sΓ , S t sΓ
P (σ ,s t) pA = P t (inr pA)

-- equalities

ass : ∀{Γ Θ Ω Δ}{σ : Sub Ω Δ}{δ : Sub Θ Ω}{ν : Sub Γ Θ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = refl

idl : ∀{Γ Δ}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
idl = refl

idr : ∀{Γ Δ}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
idr = refl

lem : {B : Set}{A : B → Set}(f g : {b : B} → ⊥ → A b) → (λ {b} → f {b}) ≡ (λ {b} → g {b})
lem {A} f g i ()

·η : ∀{Γ}{σ : Sub Γ ·} → σ ≡ ε
·η {Γ}{σ} = Sub= refl P=
  where
    P= : (λ {sΓ} → P σ {sΓ}) ≡ P (ε {Γ})
    P= i ()

module _ {A B C D : Type}{f : A → B}{g : A → C}{h : C → D} where
{-
    g     h
 A --> C --> D
f|     |inl  |inl
 v     v     v
 B -->   -->
   inr   inr          -}
  Pushoutcomp : Pushout (λ a → h (g a)) f ≡ Pushout {A = C}{D}{Pushout g f} h inl
  Pushoutcomp = ua (isoToEquiv (iso α β αβ βα))
    where
      α : Pushout (λ a → h (g a)) f → Pushout h inl
      α (inl d) = inl d
      α (inr b) = inr (inr b)
      α (push a i) = hcomp (λ j → λ { (i = i0) → inl (h (g a)) ; (i = i1) → inr (push a j) }) (push (g a) i)
      β : Pushout h inl → Pushout (λ a → h (g a)) f
      β (inl d) = inl d
      β (inr (inl c)) = inl (h c)
      β (inr (inr b)) = inr b
      β (inr (push a i)) = push a i
      β (push c i) = inl (h c)
      αβ : (x : Pushout h inl) → α (β x) ≡ x
      αβ (inl d) = refl
      αβ (inr (inl c)) = push c
      αβ (inr (inr b)) = refl
      αβ (inr (push a i)) j = sq i j i1
        where
          sq : I → I → I → Pushout h inl
          sq i j k = hcomp (λ k → λ {
              (i = i0) → push (g a) j ;
              (i = i1) → inr (push a k) ;
              (j = i0) → hfill (λ k → λ { (i = i0) → inl (h (g a)) ; (i = i1) → inr (push a k) }) (inc (push (g a) i)) k ;
              (j = i1) → inr (push a (i ∧ k)) })
            (push (g a) (i ∨ j))
      αβ (push c i) j = push c (i ∧ j)
      βα : (x : Pushout (λ a → h (g a)) f) → β (α x) ≡ x
      βα (inl d) = refl
      βα (inr b) = refl
      βα (push a i) j = sq i j i1
        where
          sq : I → I → I → Pushout (λ a → h (g a)) f
          sq i j k = hcomp (λ k → λ {
              (i = i0) → inl (h (g a)) ;
              (i = i1) → push a k ;
              (j = i0) → β (hfill (λ k → λ { (i = i0) → inl (h (g a)) ; (i = i1) → inr (push a k) }) (inc (push (g a) i)) k) ;
              (j = i1) → push a (i ∧ k) })
            (inl (h (g a)))

[∘]T : ∀{Γ Θ Δ}{A : Ty Δ}{σ : Sub Θ Δ}{δ : Sub Γ Θ} → A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T
[∘]T {Γ}{Θ}{Δ}{A}{σ}{δ} = Ty= refl (λ i {sΓ} sA → Pushoutcomp {f = Q A sA}{P σ}{P δ} i) (λ i {sΓ} sA pΓ → Q= {sΓ}{sA}{pΓ} i)
  where
    Q= : {sΓ : S Γ}{sA : S A (S σ (S δ sΓ))}{pΓ : P Γ sΓ} → PathP (λ i → Pushoutcomp {f = Q A sA}{P σ}{P δ} i) (inl pΓ) (inl pΓ)
    Q= {sΓ}{sA}{pΓ} = toPathP λ i → inl (transp (λ _ → P Γ sΓ) i pΓ)

module _ {A B : Type}{f : A → B} where
  Pushoutidr : Pushout f (λ x → x) ≡ B
  Pushoutidr = ua (isoToEquiv (iso α β αβ βα))
    where
      α : Pushout f (λ x → x) → B
      α (inl b) = b
      α (inr a) = f a
      α (push a i) = f a
      β : B → Pushout f (λ x → x)
      β = inl
      αβ : (b : B) → α (β b) ≡ b
      αβ b = refl
      βα : (c : Pushout f (λ x → x)) → β (α c) ≡ c
      βα (inl b) = refl
      βα (inr a) = push a
      βα (push a i) j = push a (i ∧ j)
  Pushoutidl : Pushout (λ x → x) f ≡ B
  Pushoutidl = ua (isoToEquiv (iso α β αβ βα))
    where
      α : Pushout (λ x → x) f → B
      α (inl a) = f a
      α (inr b) = b
      α (push a i) = f a
      β : B → Pushout (λ x → x) f
      β = inr
      αβ : (b : B) → α (β b) ≡ b
      αβ b = refl
      βα : (c : Pushout (λ x → x) f) → β (α c) ≡ c
      βα (inl b) = sym (push b)
      βα (inr a) = refl
      βα (push a i) j = push a (i ∨ ~ j)

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T {Γ}{A} = Ty= refl (λ i {sΓ} sA → Pushoutidl {P Γ sΓ}{P A sA}{Q A sA} i) (λ i {sΓ} sA pΓ → Q= {sΓ}{sA}{pΓ} i)
  where
    Q= : {sΓ : S Γ}{sA : S A sΓ}{pΓ : P Γ sΓ} → PathP (λ i → Pushoutidl {P Γ sΓ}{P A sA}{Q A sA} i) (inl pΓ) (Q A sA pΓ)
    Q= {sΓ}{sA}{pΓ} = toPathP λ i → transp (λ _ → P A sA) i (Q A sA pΓ)

[∘]t : ∀{Γ Θ Δ}{A : Ty Δ}{t : Tm Δ A}{σ : Sub Θ Δ}{δ : Sub Γ Θ} → PathP (λ i → Tm Γ ([∘]T {A = A}{σ}{δ} i)) (t [ σ ∘ δ ]t) (t [ σ ]t [ δ ]t)
[∘]t {Γ}{Θ}{Δ}{A}{t}{σ}{δ} = Tm= ([∘]T {Γ}{Θ}{Δ}{A}{σ}{δ}) refl {!!} {!!}

[id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → PathP (λ i → Tm Γ ([id]T {Γ}{A} i)) (t [ id ]t) t
[id]t {Γ}{A}{t} = Tm= ([id]T {Γ}{A}) refl P= {!!}
  where
    P= : PathP (λ i → {sΓ : S Γ} → P ([id]T {Γ}{A} i) (S t sΓ) → P Γ sΓ) (P (t [ id ]t)) (P t)
    P= i pA = {!!}

▹β₁ : ∀{Γ}{Δ}{A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → p {Δ}{A} ∘ (_,s_ {A = A} σ t) ≡ σ
▹β₁ {Γ}{Δ}{A}{σ}{t} = Sub= refl (λ i {sΔ} pΔ → hcomp (λ j → λ { (i = i0) → P t (push {f = P σ}{Q A (S t sΔ)} pΔ j) ; (i = i1) → P σ pΔ }) (Q t (~ i) (P σ pΔ)))

▹β₂ : ∀{Γ}{Δ}{A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} →
  PathP (λ i → Tm Γ (hcomp (λ j → λ { (i = i0) → [∘]T {A = A}{p {Δ}{A}}{_,s_ {A = A} σ t} j ; (i = i1) → A [ σ ]T }) (A [ ▹β₁ {Γ}{Δ}{A}{σ}{t} i ]T)))
        (q {Δ}{A} [ _,s_ {A = A} σ t ]t)
        t
▹β₂ {Γ}{Δ}{A}{σ}{t} = Tm=
  (λ i → hcomp (λ j → λ { (i = i0) → [∘]T {A = A}{p {Δ}{A}}{_,s_ {A = A} σ t} j ; (i = i1) → A [ σ ]T }) (A [ ▹β₁ {Γ}{Δ}{A}{σ}{t} i ]T))
  {!!}
  {!!}
  {!!}

▹η : ∀{Γ}{Δ}{A}{σ : Sub Γ (Δ ▹ A)} → _,s_ {A = A} (p {Δ}{A} ∘ σ) (subst (Tm Γ) (sym ([∘]T {A = A}{p {Δ}{A}}{σ})) (q {Δ}{A} [ σ ]t)) ≡ σ
▹η {Γ}{Δ}{A}{σ} = Sub= {!!} {!!}

-- equalities
-- Sub= : ∀{Γ Δ}{σ₀ σ₁ : Sub Γ Δ}(S= : S σ₀ ≡ S σ₁)(P= : _≡_ {A = {sΓ : Con.S Γ} → P Δ (S σ₁ sΓ) → P Γ sΓ} (subst (λ z → {sΓ : Con.S Γ} → P Δ (z sΓ) → P Γ sΓ) S= (P σ₀)) (P σ₁)) → σ₀ ≡ σ₁
-- Sub= refl p = {!p!}

Σ' : {Γ : Con}(A : Ty Γ)(B : Ty (Γ ▹ A)) → Ty Γ
S (Σ' A B) sΓ = Σ (S A sΓ) λ sA → S B (sΓ , sA)
P (Σ' A B) (sA , sB) = P B sB
Q (Σ' A B) (sA , sB) pΓ = Q B sB (Q A sA pΓ)

_,'_ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}(u : Tm Γ A)(v : Tm Γ (B [ _,s_ {A = A} id (subst (Tm Γ) (sym [id]T) u) ]T)) → Tm Γ (Σ' A B)
S (u ,' v) sΓ = S u sΓ , {!S v sΓ!}
P (u ,' v) pB = P v {!!} -- (inr pB)
Q (u ,' v) = {!!}

proj₁' : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm Γ (Σ' A B) → Tm Γ A
S (proj₁' t) sΓ = fst (S t sΓ)
P (proj₁' {A = A}{B} t) {sΓ} pA = P t (Q B (snd (S t sΓ)) pA)
Q (proj₁' t) = Q t

proj₂' : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}(t : Tm Γ (Σ' A B)) → Tm Γ (B [ _,s_ {A = A} id (subst (Tm Γ) (sym ([id]T {A = A})) (proj₁' {Γ}{A}{B} t)) ]T)
S (proj₂' t) sΓ = {! snd (S t sΓ) !}
P (proj₂' t) = {!!}
Q (proj₂' t) = {!!}

-- constant types

K' : Con → Ty ·
S (K' Δ) = λ _ → S Δ
P (K' Δ) sΔ = P Δ sΔ
Q (K' Δ) sΔ ()

K : Con → {Γ : Con} → Ty Γ
K A = K' A [ ε ]T

-- products

_×C_ : Con → Con → Con
S (Γ ×C Δ) = S Γ × S Δ
P (Γ ×C Δ) (sΓ , sΔ) = P Γ sΓ ⊎ P Δ sΔ

_,×_ : ∀{Γ Δ Θ} → Sub Γ Δ → Sub Γ Θ → Sub Γ (Δ ×C Θ)
S (σ ,× δ) sΓ = S σ sΓ , S δ sΓ
P (σ ,× δ) (inl pΔ) = P σ pΔ
P (σ ,× δ) (inr pΘ) = P δ pΘ

fst× : ∀{Γ Δ Θ} → Sub Γ (Δ ×C Θ) → Sub Γ Δ
S (fst× σ) sΓ = fst (S σ sΓ)
P (fst× σ) pΔ = P σ (inl pΔ)

snd× : ∀{Γ Δ Θ} → Sub Γ (Δ ×C Θ) → Sub Γ Θ
S (snd× σ) sΓ = snd (S σ sΓ)
P (snd× σ) pΘ = P σ (inr pΘ)

-- exponentials

_⇒C_ : Con → Con → Con
S (Γ ⇒C Δ) = Σ (S Γ → S Δ) λ Sσ → ∀{sΓ} → P Δ (Sσ sΓ) → ⊤ ⊎ P Γ sΓ
P (Γ ⇒C Δ) (Sf , Pf) = Σ (S Γ) λ sΓ → Σ (P Δ (Sf sΓ)) λ pΔ → Pf {sΓ} pΔ ≡ inl tt
{-
lamC : ∀{Γ Δ Θ} → Sub (Γ ×C Δ) Θ → Sub Γ (Δ ⇒C Θ)
S (lamC σ) sΓ sΔ = S σ (sΓ , sΔ) , λ pΘ → elim⊎ (λ _ → inl tt) inr (P σ pΘ)
P (lamC σ) (sΔ , pΘ , e) = {!!} -- elim⊎ (λ pΓ → pΓ) (λ pΔ → {!!}) (P σ pΘ)
-}
-- conversion to presheaves

⟦_⟧C : Con → P.Con
P.ob  ⟦ mk SΓ PΓ ⟧C X = Σ SΓ λ sΓ → PΓ sΓ → X
P.mor ⟦ mk SΓ PΓ ⟧C f (sΓ , g) = sΓ , λ pΓ → f (g pΓ)
P.∘   ⟦ mk SΓ PΓ ⟧C = refl
P.id  ⟦ mk SΓ PΓ ⟧C = refl

⟦_⟧s : ∀{Γ Δ} → Sub Γ Δ → P.Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
P.ob  ⟦ mk Sσ Pσ ⟧s (sΓ , f) = Sσ sΓ , λ pΔ → f (Pσ pΔ)
P.mor ⟦ mk Sσ Pσ ⟧s f (sΓ , g) = refl

⟦_⟧T : ∀{Γ} → Ty Γ → P.Ty ⟦ Γ ⟧C
P.ob  ⟦ mk SA PA QA ⟧T {X} (sΓ , f) = Σ (SA sΓ) λ sA → Σ (PA sA → X) λ g → f ≡ λ pΓ → g (QA sA pΓ)
P.mor ⟦ mk SA PA QA ⟧T h (sA , g , e) = sA , (λ pA → h (g pA)) , cong (λ z pΓ → h (z pΓ)) e
P.∘   ⟦ mk SA PA QA ⟧T = {!!}
P.id  ⟦ mk SA PA QA ⟧T = {!!}
