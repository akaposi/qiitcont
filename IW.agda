{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Agda.Builtin.Unit
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels
open import Cubical.Data.Empty
open import Cubical.Data.Unit
open import Cubical.Data.Nat
open import Cubical.Data.Sigma
open import Cubical.Data.Sum

module IW where

module WWW  where

  data W (I : Type)(S : I → Type)(P : (i : I) → S i → I → Type) : I → Type where
    sup : (i : I)(s : S i)(f : (j : I) → P i s j → W I S P j) → W I S P i

  module _ (I : Type)(S : I → Type)(P : (i : I) → S i → I → Type) where
    data W='' : Σ I (λ i → W I S P i × W I S P i) → Type where
      eq : {i : I}(s : S i)(f f' : (j : I) → P i s j → W I S P j)
           (p= : (j : I)(p : P i s j) → W='' (j , f j p , f' j p)) →
           W='' (i , sup i s f , sup i s f')

    data W=' : Σ I (λ i → W I S P i × W I S P i) → Type where
      eq : {i : I}(s : S i)(s' : S i)(f : (j : I) → P i s j → W I S P j)(f' : (j : I) → P i s' j → W I S P j)
           (s= : s ≡ s')
           (p= : (j : I)(p : P i s j)(p' : P i s' j)(p= : PathP (λ ii → P i (s= ii) j) p p') → W=' (j , f j p , f' j p')) →
           W=' (i , sup i s f , sup i s' f')
    
    W= : Σ I (λ i → W I S P i × W I S P i) → Type
    W= = W (Σ I λ i → W I S P i × W I S P i) (λ { (i , sup i s f , sup i s' f') → s ≡ s' }) λ { (i , sup i s f , sup i s' f') e → {!!} }

  -- vectors

  postulate A : Type

  module first where
    Vec : ℕ → Type
    Vec = W ℕ
      (λ { zero → ⊤      ; (suc _) → A })
      (λ { zero _ _ → ⊥  ; (suc n) a m → n ≡ m })

    nil : Vec zero
    nil = sup zero tt (λ _ ())

    cons : {n : ℕ} → A → Vec n → Vec (suc n)
    cons {n} a as = sup (suc n) a (λ j e → transport (λ i → Vec (e i)) as)

    elimVec : (P : (n : ℕ) → Vec n → Type) → P zero nil → ((n : ℕ)(a : A)(as : Vec n)(p : P n as) → P (suc n) (cons a as)) → (n : ℕ)(as : Vec n) → P n as
    elimVec P pn pc zero    as = transport (λ i → P zero {!!}) pn
    elimVec P pn pc (suc n) as = {!!}

  module second where
    Vec : ℕ → Type
    Vec = W ℕ
      (λ n → (n ≡ zero) ⊎ Σ ℕ λ m → A × (n ≡ suc m))
      (λ { n (inl e) _ → ⊥ ; n (inr (m , a , e)) i → (i ≡ m) })

    nil : Vec zero
    nil = sup zero (inl refl) (λ _ ())

    cons : {n : ℕ} → A → Vec n → Vec (suc n)
    cons {n} a as = sup (suc n) (inr (n , a , refl)) (λ n' e → transport (λ i → Vec (e (~ i))) as)

module third (A : Type)(N : Type)(z : N)(s : N → N) where

  data Vec (n : N) : Type where
    nil  : n ≡ z → Vec n
    cons : {m : N} → n ≡ s m → A → Vec m → Vec n

  transpVec : {n n' : N} → n ≡ n' → Vec n → Vec n'
  transpVec e (nil e')  = nil (sym e ∙ e')
  transpVec e (cons e' a as) = cons (sym e ∙ e') a as

module tree (A : Type)(N : Type)(z : N)(comb : N → N → N) where
  data T (n : N) : Type where
    leaf : n ≡ z → T n
    node : {m m' : N} → T m → T m' → n ≡ comb m m' → T n

  transpV : {n n' : N}(n= : n ≡ n') → T n → T n'
  transpV n= (leaf e) = leaf (sym n= ∙ e)
  transpV n= (node t t' e) = node t t' (sym n= ∙ e)

open import Cubical.Data.Bool
module tree2 (A : Type)(N : Type)(z : N)(comb : (Bool → N) → N) where
  data T (n : N) : Type where
    leaf : n ≡ z → T n
    node : (ns : Bool → N) → ((b : Bool) → T (ns b)) → n ≡ comb ns → T n

  transpV : {n n' : N}(n= : n ≡ n') → T n → T n'
  transpV n= (leaf e) = leaf (sym n= ∙ e)
  transpV n= (node ns f e) = node ns f (sym n= ∙ e)

module inftree (A : Type)(M : Type)(N : Type)(z : N)(comb : (M → N) → N) where
  -- M-branching tree indexed by N
  data T (n : N) : Type where
    leaf : n ≡ z → T n
    node : (f : M → N)(g : (m : M) → T (f m)) → n ≡ comb f → T n

  transpT : {n n' : N}(n= : n ≡ n') → T n → T n'
  transpT n= (leaf e) = leaf (sym n= ∙ e)
  transpT n= (node f g e) = node f g (sym n= ∙ e)

module WW (I : Type)(S : I → Type)(P : (i : I) → S i → I → Type) where

  data W (i0 : I) : Type where
    sup : (i : I)(e : i ≡ i0)(s : S i)(f : (j : I) → P i s j → W j) → W i0
  
  transpW : {i i' : I}(i= : i ≡ i') → W i → W i'
  transpW {i}{i'} i= (sup i'' e s f) = sup _ (e ∙ i=) s f

{-
  data Vec= : {n n' : ℕ}(n= : ℕ= n n') → Vec n → Vec n' → Type where
    nil=  : Vec= zero= nil nil
    cons= : {n n' : ℕ}(n= : ℕ= n n'){a a' : A}(a= : a ≡ a'){as : Vec n}{as' : Vec n'}(as= : PathP (λ i → Vec (dec n= i)) as as') → Vec= (suc= n=) (cons a as) (cons a' as')
-}
