{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Agda.Builtin.Cubical.Sub hiding (Sub)
open import Agda.Builtin.Unit
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Data.Empty
open import Cubical.Data.Unit
open import Cubical.Data.Nat
open import Cubical.Data.Sigma
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Univalence

postulate -- does not need to be inductive
  type  : Type
  uₚ    : type
  elₚ   : ℕ → type
  arrₚ  : type → type → type
  _[_]ₚ : type → ℕ → type
  loopₚ : uₚ ≡ uₚ
  
data isN : type → Set where
  u   : isN uₚ
  el  : {n : ℕ} → isN (elₚ n)
  arr : {a b : type} → isN a → isN b → isN (arrₚ a b)

data _~_ : ∀{a b} → isN a → isN b → Type where
  u   : u ~ u
  el  : {m n : ℕ} → m ≡ n → el {m} ~ el {n}
  arr : ∀{a b a' b'}{aa : isN a}{bb : isN b}{aa' : isN a'}{bb' : isN b'} →
        aa ~ aa' → bb ~ bb' → arr aa bb ~ arr aa' bb'

refl~ : ∀{a}(aa : isN a) → aa ~ aa
refl~ u = u
refl~ el = el refl
refl~ (arr aa bb) = arr (refl~ aa) (refl~ bb)

enc : ∀{a b}{aa : isN a}{bb : isN b}(e : _≡_ {A = Σ type isN} (a , aa) (b , bb)) → aa ~ bb
enc {a}{b}{aa}{bb} = J (λ bbb eee → aa ~ snd bbb) (refl~ aa)

encRefl : ∀{a}(aa : isN a) → enc (refl {x = a , aa}) ≡ refl~ aa
encRefl {a} aa = JRefl (λ (bbb : Σ type isN) eee → aa ~ snd bbb) (refl~ aa)

dec : ∀{a b}{aa : isN a}{bb : isN b} → aa ~ bb → (a , aa) ≡ (b , bb)
dec u = refl
dec (el e) i = elₚ (e i) , el {e i}
dec (arr ee ee') i = arrₚ (fst (dec ee i)) (fst (dec ee' i)) , arr (snd (dec ee i)) (snd (dec ee' i))

decRefl : ∀{a}(aa : isN a) → dec (refl~ aa) ≡ refl
decRefl u = refl
decRefl el = refl
decRefl (arr aa aa') i j = arrₚ (fst (decRefl aa i j)) (fst (decRefl aa' i j)) , arr (snd (decRefl aa i j)) (snd (decRefl aa' i j))

decenc : {aaa bbb : Σ type isN}(eee : aaa ≡ bbb) → dec (enc eee) ≡ eee
decenc {aaa} = J (λ bbb eee → dec (enc eee) ≡ eee) ((λ i → dec (encRefl (snd aaa) i)) ∙ decRefl (snd aaa))

enc-arr : {aaa aaa' bbb : Σ type isN}(eee : aaa ≡ bbb){bbb' : Σ type isN}(eee' : aaa' ≡ bbb') → enc (λ i → arrₚ (fst (eee i)) (fst (eee' i)) , arr (snd (eee i)) (snd (eee' i))) ≡ arr (enc eee) (enc eee')
enc-arr {aaa}{aaa'} = J
  (λ bbb eee → {bbb' : Σ type isN}(eee' : aaa' ≡ bbb') → enc (λ i → arrₚ (fst (eee i)) (fst (eee' i)) , arr (snd (eee i)) (snd (eee' i))) ≡ arr (enc λ i → (fst (eee i) , snd (eee i))) (enc eee'))
  (J
    (λ bbb' eee' → enc (λ i → arrₚ (fst aaa) (fst (eee' i)) , arr (snd aaa) (snd (eee' i))) ≡ arr (enc (λ _ → aaa)) (enc λ i → (fst (eee' i) , snd (eee' i))))
    (encRefl (arr (snd aaa) (snd aaa')) ∙ sym λ i → arr (encRefl (snd aaa) i) (encRefl (snd aaa') i)))

encdec : ∀{a b}{aa : isN a}{bb : isN b}(ee : aa ~ bb) → enc (dec ee) ≡ ee
encdec u = encRefl u
encdec (el {m}{n} e) = J (λ n e → enc (λ i → elₚ (e i) , el) ≡ el e) (encRefl (el {m})) e
encdec (arr ee ee') = enc-arr (dec ee) (dec ee') ∙ λ i → arr (encdec ee i) (encdec ee' i)

fruit : ∀{aaa bbb : Σ type isN} → (aaa ≡ bbb) ≡ (snd aaa ~ snd bbb)
fruit = ua (isoToEquiv (iso enc dec encdec decenc))

~uip : ∀{a b}(aa : isN a)(bb : isN b)(ee ee' : aa ~ bb) → ee ≡ ee'
~uip u u u u = refl
~uip (el {m}) (el {n}) (el e) (el e') i = _~_.el {m}{n} (isSetℕ m n e e' i)
~uip (arr aa₀₀ aa₀₁) (arr aa₁₀ aa₁₁) (arr ee~₀ ee~₁) (arr ee₀~ ee₁~) i = arr (~uip aa₀₀ aa₁₀ ee~₀ ee₀~ i) (~uip aa₀₁ aa₁₁ ee~₁ ee₁~ i)

FRUIT : isSet (Σ type isN)
FRUIT A B e e' = transport (λ i → (transport-filler fruit e (~ i) ≡ transport-filler fruit e' (~ i))) (~uip (snd A) (snd B) (transport fruit e) (transport fruit e'))
