{-# OPTIONS --rewriting --cubical #-}
{-
open import Data.Nat.Base
open import Relation.Binary.PropositionalEquality
-}
open import Cubical.Foundations.Prelude
open import Cubical.Data.Nat
{-# BUILTIN REWRITE _≡_ #-}

module mike where

module indexedVersion where

  data _≡ℕ_ : ℕ → ℕ → Set where
    zero≡ : zero ≡ℕ zero
    suc≡ : ∀ {m n} → m ≡ℕ n → suc m ≡ℕ suc n

  reflℕ : ∀ {m} → m ≡ℕ m
  reflℕ {zero} = zero≡
  reflℕ {suc m} = suc≡ (reflℕ {m})
{-
      a' ? b'
       |   |
       a---b
-}
  coe≡ℕ : ∀{a a'}(e : a ≡ℕ a'){b b'}(f : b ≡ℕ b') → a ≡ℕ b → a' ≡ℕ b'
  coe≡ℕ zero≡ zero≡ zero≡ = zero≡
  coe≡ℕ zero≡ (suc≡ f) ()
  coe≡ℕ (suc≡ e) zero≡ ()
  coe≡ℕ (suc≡ e) (suc≡ f) (suc≡ u) = suc≡ (coe≡ℕ e f u)

  module _(M : ℕ → Set)(mz : M zero)(ms : ∀ {m} → M m → M (suc m))
          (M≡ : ∀ {m n} → m ≡ℕ n → M m → M n → Set)
          (reflM : ∀ {m} (mm : M m) → M≡ (reflℕ {m}) mm mm)
  {-
          (ap-mz : M≡ (reflℕ {zero}) mz mz)
          (ap-ms : ∀ {m n}(p : m ≡ℕ n) → 
                     {mm : M m}{mn : M n} → M≡ p mm mn →
                     M≡ (suc≡ p) (ms mm) (ms mn))
          (ap-mz-refl : ap-mz ≡ reflM mz)
          (ap-ms-refl : ∀ {m}(m : M m) → ap-ms reflℕ (reflM m) ≡ reflM (ms m))
  -}
          where

    postulate
      ap-mz : M≡ (reflℕ {zero}) mz mz
      ap-ms : ∀ {m n}(p : m ≡ℕ n) → 
                     {mm : M m}{mn : M n} → M≡ p mm mn →
                     M≡ (suc≡ p) (ms mm) (ms mn)

      ap-mz-refl : ap-mz ≡ reflM mz
      ap-ms-refl : ∀ {m}(m : M m) → ap-ms reflℕ (reflM m) ≡ reflM (ms m)

    {-# REWRITE ap-mz-refl ap-ms-refl #-}

    Eℕ : (n : ℕ) → M n
    Eℕ zero = mz
    Eℕ (suc n) = ms (Eℕ n)

    ap-Eℕ : ∀ {m n}(p : m ≡ℕ n) → M≡ p (Eℕ m) (Eℕ n)
    ap-Eℕ zero≡ = ap-mz
    ap-Eℕ (suc≡ p) = ap-ms p (ap-Eℕ p)

    ap-Eℕ-refl : ∀ {m} → ap-Eℕ (reflℕ {m}) ≡ reflM (Eℕ m)
    ap-Eℕ-refl {zero} = refl
    ap-Eℕ-refl {suc m} = cong (ap-ms reflℕ) (ap-Eℕ-refl {m})
     -- rewrite (ap-Eℕ-refl {m})
     --          = refl

module paramVersion where

  data _≡ℕ_ (m n : ℕ) : Set where
    zero≡ : (em : m ≡ℕ 0)(en : n ≡ℕ 0) → m ≡ℕ n
    -- suc≡ : ∀ {m n} → m ≡ℕ n → suc m ≡ℕ suc n
  
  zero≡' : 0 ≡ℕ 0
  zero≡' = zero≡ {!!} {!!}

module Vec where

  data Vec (n : ℕ) : Set where
    nil : (e : n ≡ 0) → Vec n

  data ≡Vec : {m n : ℕ}(e : m ≡ n)(xs : Vec m)(ys : Vec n) → Set where
    nil= : ∀{m m'}(me : m ≡ m'){e : m ≡ 0}{e' : m' ≡ 0}(ee : PathP (λ i → me i ≡ 0) e e') → ≡Vec me (nil e) (nil e') 

  reflVec : {m : ℕ}(xs : Vec m) → ≡Vec refl xs xs
  reflVec (nil e) = nil= refl refl

  coeVec : ∀{m n} → m ≡ n → Vec m → Vec n
  coeVec e (nil e') = nil {!sym e ∙ e'!} -- <- using coe ℕ=

  

module Vec' where

  data Vec' : ℕ → Set where
    nil : Vec' zero
    -- coeVec' : ... we have to build it in

  data ≡Vec' : {m n : ℕ}(e : m ≡ n)(xs : Vec' m)(ys : Vec' n) → Set where
    nil≡ : ≡Vec' refl nil nil

  coeVec' : ∀{m n} → m ≡ n → Vec' m → Vec' n
  coeVec' e nil = {!!}
