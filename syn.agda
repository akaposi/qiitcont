{-# OPTIONS --cubical #-}
open import Cubical.Core.Everything
open import Cubical.Foundations.Prelude

data U : Set₁
data El : U → Set₁

TT : Set₁
variable A B : TT
Ext : TT → TT → Set₁
Con : TT → Set₁
variable Γ Δ : Con A
Constr : TT → Set₁
variable C : Constr A
Ty RTy : Con A → Set₁
variable σ τ : Ty Γ
variable ρ : RTy Γ
Tm : (Γ : Con A) → Ty Γ → Set₁
Tms : Con A → Con A → Set₁
variable ts : Tms Δ Γ

data U where
  tt : U
  ext : TT → TT → U
  constr : TT → U
  con : TT → U
  ty rty : Con A → U
  tm : (Γ : Con A) → Ty Γ → U
  tms : Con A → Con A → U

TT = El tt
Ext A B = El (ext A B)
Con A = El (con A)
Constr A = El (constr A)
Ty Γ = El (ty Γ)
RTy Γ = El (rty Γ)
Tm Γ σ = El (tm Γ σ)
Tms Γ Δ = El (tms Γ Δ)

data El where

  ε : TT
  _+_ : (A : TT) → Constr A → TT
-- inline Constr ?
  _<_>c : Constr B → Ext A B → Constr A

  idE : Ext A A
  fstE : Ext A (B + C) → Ext A B
--  sndE : Ext A (B + C) → Constr A

-- A+C => A+C , A+C => A
-- Ty A ---> Ty (A + C)

--  fstE : Ext (A , C) A

  • : Con A
  _,_ : (Γ : Con A) → Ty Γ → Con A

  _⇒_ : (Γ : Con A) → RTy Γ → Constr A

  _<_>C : Con B → Ext A B → Con A
  _<_>T : (σ : Ty {A = B} Γ) → (e : Ext A B) → Ty (Γ < e >C)
  _<_>R : (σ : RTy {A = B} Γ) → (e : Ext A B) → RTy (Γ < e >C)
  _<_>t : (t : Tm {A = B} Γ σ) → (e : Ext A B) → Tm (Γ < e >C) (σ < e >T)

  _[_]T : Ty Γ → Tms Δ Γ → Ty Δ
  _[_]R : RTy Γ → Tms Δ Γ → RTy Δ  
  _[_]t : Tm Γ σ → (ts : Tms Δ Γ) → Tm Δ (σ [ ts ]T)

  uu : RTy Γ
  emb : RTy Γ → Ty Γ
  el : Tm Γ (emb uu) → RTy Γ

  uu[] : uu [ ts ]R ≡ uu 

-- ℕ → U ==> pi ℕ (emb uu) 
  pi : (X : Set) → (X → Ty Γ) → Ty Γ
  lam : {X : Set}(f : X → Ty Γ) → ((x : X) → Tm Γ (f x)) → Tm Γ (pi X f)
  app : {X : Set}(f : X → Ty Γ) → Tm Γ (pi X f) → ((x : X) → Tm Γ (f x))

-- ℕ 
  par : (X : Set) → Ty Γ
  put : {X : Set} → X → Tm Γ (par X)
  appP : {X : Set} → (X → Tm Γ σ) → Tm Γ (par X) → Tm Γ σ

  cnstr : Tm {A = A + (_⇒_ {A = A} Γ ρ)}
             (Γ < fstE idE >C) (emb (ρ < fstE idE >R))

  idt : Tms Γ Γ
  fstt : Tms Γ (Δ , σ) → Tms Γ Δ
  sndt : {Γ Δ : Con A}{σ : Ty Δ} → (ts : Tms Γ (Δ , σ)) → Tm Γ (σ [ fstt ts ]T)
  _,t_ : (ts : Tms Γ Δ)(t : Tm Γ (σ [ ts ]T)) → Tms Γ (Δ , σ)

  eq : {ρ : RTy Γ}(a b : Tm Γ (emb ρ)) → Ty Γ
  rfl : {Γ : Con A}{ρ : RTy Γ}(a : Tm Γ (emb ρ)) → Tm Γ (eq a a)
  eqR : {Γ : Con A}{ρ : RTy Γ}(a b : Tm Γ (emb ρ)) → Tm Γ (eq a b) → a ≡ b

{-
 Fam U : Set
     El : U → Set

 vec : ℕ → U
 nil : El (vec 0)
 cons : (n : ℕ) → A → El (vec n) → El (vec (suc n))

 ε + vec : (• , par ℕ) ⇒ UU 
 + (• ⇒ el (cnstr vec [ 0 ])
 + (• , n : par ℕ , par A , el (cnstr vec [n]) ⇒ el (cnstr vec [n+1])

[n+1] = appP (λ x → put (x + 1)) (var n)
-}

{- Semantics

⟦ TT ⟧ = QIIT -- locally pres cats?
⟦ Ext A B ⟧ = adjoint functors ⟦ A ⟧ ⟦ B ⟧
⟦ Con A ⟧ = Cont ⟦ A ⟧
⟦ Ty Γ ⟧ = Cont (∫ ⟦ Γ ⟧)
⟦ RTy Γ ⟧ = ? (cont with the same shapes as ⟦ Γ ⟧) 
⟦ Tm{A} Γ σ ⟧ = DCont_⟦A⟧ ⟦ Γ ⟧ ⟦ σ ⟧
⟦ Tms{A} Γ Δ ⟧ = Cont_⟦A⟧ ⟦ Γ ⟧ ⟦ Δ ⟧

⟦ ε ⟧ = FAM
⟦ A + (Γ ⇒ ρ) ⟧ = ⟦ A ⟧ × (DCont_⟦A⟧ ⟦ Γ ⟧ ⟦ ρ ⟧)   

-- extend the syntax with equations
-- syn => sem : define the semantics in particular the free-ext functor 
-- sem => syn : translate the containers back into syntax
-- and hsow that they are inverses
-- Set^-> Fam, Cat with adjoint functor to Fam.


-}

