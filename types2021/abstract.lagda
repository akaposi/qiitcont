% easychair.tex,v 3.5 2017/03/15

\documentclass{easychair}
%\documentclass[EPiC]{easychair}
%\documentclass[EPiCempty]{easychair}
%\documentclass[debug]{easychair}
%\documentclass[verbose]{easychair}
%\documentclass[notimes]{easychair}
%\documentclass[withtimes]{easychair}
%\documentclass[a4paper]{easychair}
%\documentclass[letterpaper]{easychair}
%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

\usepackage{doc}

% use this if you have a long article and want to create an index
% \usepackage{makeidx}

% In order to save space or manage large tables or figures in a
% landcape-like text, you can use the rotating and pdflscape
% packages. Uncomment the desired from the below.
%
% \usepackage{rotating}
% \usepackage{pdflscape}

% Some of our commands for this guide.
%
\newcommand{\easychair}{\textsf{easychair}}
\newcommand{\miktex}{MiK{\TeX}}
\newcommand{\texniccenter}{{\TeX}nicCenter}
\newcommand{\makefile}{\texttt{Makefile}}
\newcommand{\latexeditor}{LEd}

%\makeindex

%% Front Matter
%%
% Regular title as in the article class.
%
\title{A container model of type theory%
\thanks{ Supported by USAF, Airforce office for scientific research, award FA9550-16-1-0029.}}

% Authors are joined by \and. Their affiliations are given by \inst, which indexes
% into the list defined using \institute
%
\author{
Thorsten Altenkirch\inst{1}%\thanks{text}
\and
Ambrus Kaposi\inst{1}\inst{2}%\thanks{text}
}

% Institutes for affiliations are also joined by \and,
\institute{
  University of Nottingham,
  Nottingham, United Kingdom\\
  %\email{thorsten.altenkirch@nottingham.ac.uk}
\and
   Eötvös Loránd University,
   Budapest, Hungary\\
   %\email{akaposi@inf.elte.hu}
 }

%  \authorrunning{} has to be set for the shorter version of the authors' names;
% otherwise a warning will be rendered in the running heads. When processed by
% EasyChair, this command is mandatory: a document without \authorrunning
% will be rejected by EasyChair

\authorrunning{Altenkirch and Kaposi}

% \titlerunning{} has to be set to either the main title or its shorter
% version for the running heads. When processed by
% EasyChair, this command is mandatory: a document without \titlerunning
% will be rejected by EasyChair
\titlerunning{A container model of type theory}

\begin{document}

\maketitle

% \begin{abstract}
% Do we need an abstract?
% \end{abstract}

%------------------------------------------------------------------------------
\section*{Introduction}
\label{sect:introduction}

We present a model of type theory where types are interpreted as
containers \cite{abbottthesis}  aka polynomial functors. The motivation for this
construction is to provide a container semantics for
inductive-inductive types and quotient inductive-inductive types.

Consider the core of our usual example of an inductive-inductive type:
\begin{code}
data Con : Set
data Ty : Con → Set

_,_ : (Γ : Con) → Ty Γ → Con
Π : (Γ : Con)(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
\end{code}
Since the constructor |Π| uses the previously defined constructor
|_,_| in its domain there is no hope of a functorial semantics where an
inductive type is the initial algebra of a container. Instead we have
to interpret the domain of a constructor as a functor |L| from the category of
algebras induced by the previous constructors to |Set|, and the
codomain a functor from the category of elements of |L| to |Set|. 
The semantics of a constructor with regard to a fixed algebra |X| is given by
| (x : L X) → R (X , x)|. This is explained in detail in
\cite{altenkirch2018quotient} where |L| is an arbitrary functor and 
types are interpreted using the usual presheaf semantics of type theory.

Such a functorial semantics is too generous because there are
many functors which do not have initial algebras. Hence we want to use
containers to model strict positivity semantically. As a first step we
show that containers do form a model of basic type theory, i.e.\ a
category with families.

Tamara von Glehn also presents a model of type theory using polynomial
functors \cite{vonglehn} using comprehension categories as notion of
model. The same model was presented by Atkey \cite{atkey} and by Kovács \cite{kovacs} using categories with families (CwFs). This model has
the same contexts and substitutions as ours but different types and
terms (see below).

In this abstract when we write |Set| we mean Agda's universe of types
and we assume uniqueness of identity proofs (see also the Discussion).

\section*{The model}
\label{sec:model}

A container (or polynomial functor) is given by a set of shapes |S :
Set| and a family of positions |P : S → Set|. This gives rise to a
functor |S ◁ P : Set → Set| which on objects is given by
|(S ◁ P) X = Σ s : S . P s → X|.  Given containers |S ◁ P| and |T ◁ Q|
a morphism is given by a function on shapes |f : S → T| and a family
of functions on positions |g : (s : S) → Q (f s) → P s| --- note the
change of direction. This gives rise to a natural transformation
|f ◁ g : (X : Set) → (S ◁ P)  X → (T ◁ Q) X| given by 
|(f ◁ g) X (s , p) = (f s, λ s → p ∘ g s)|. Using the Yoneda lemma
we can show that every natural transformation between containers
arises this way  (i.e.\ the evaluation functor from the category of
containers to the functor category is full and faithful).

We can generalize set-containers to containers over an arbitrary category |ℂ|, i.e.\ covariant functors |ℂ → Set| using |S
: Set| and |P : S → ℂ| and |(S ◁ P) X
= Σ s : S . ℂ(P s, X)| and morphisms are given by |f : S → T| and |g :
(s : S) → ℂ(Q (f s), P s)| with the same definition of the natural
transformation.

We define a category with families (CwF) which are the algebras of an
intrinsic presentation of Type Theory as given in \cite{altenkirch2016type}.
The objects corresponding to
contexts are set containers and the morphisms are container
morphisms. We write |Con| for this category. Below we sketch some
aspects of the construction, for details please check our (incomplete) Agda
formalisation \cite{agda-form}.

To interpret types we define a functor |Ty : Con → Set₁| on objects:
given |Γ : Con|, an |A : Ty Γ| is given by a container |A : ∫ Γ →
Set|. Here |∫ Γ| is the category of elements of |Γ| with objects
|Σ X : Set . Γ X| and morphisms |∫ Γ ((X , x) , (Y , y)| are given by
a function |f : X → Y| such that |Γ f x = y|.

In contrast, a type in von Glehn's model \cite{vonglehn} over a
context |Γ = S ◁ P| is a container |A : ∫ (Const S) → Set| where
|Const S| is the constant |S| presheaf. Hence types there are
dependent only on shapes, but not positions.

The interpretation of terms is given by |Tm : ∫ Ty → Set|. On an
object of |∫ Ty|, i.e.\ a |Γ : Con| and |A : ∫ Γ → Set| a term is
given by a \emph{dependent natural transformation} |(X : Set)(x : Γ X)
→ A (X , x)|. Assuming that |Γ = S-Γ ◁ P-Γ| and |A = S-A ◁ P-A| using
the \emph{dependent Yoneda lemma} we can show that this
corresponds to |f : S-Γ → S-A| and |g : (s : S-A) → ∫ Γ (P-A s ,
(P-Γ (P-A-s s) , P-A-s s , id))|. This can be further simplified using dependent
types --- see our Agda code.

Given |A : Ty Γ| we write |P-A-X : S-A → Set|, |P-A-s : S-A → S-Γ| and
|P-A-g : (s : S-A) → P-Γ (P-A-s s) → P-A-X s| for the projections of 
|P-A : S-A → ∫ Γ|.

The empty context is given by the terminal object in |Con| which is |1
◁ 0|. Assuming a |Γ : Con| and |A : Ty Γ| we construct the context extension
|Γ , A| as |S-A ◁ P-A-X|. We can verify the universal
property --- see the Agda code.

The definition of type substitution requires pushouts which can be
defined using a quotient inductive type (QIT). That is given
| f ◁ g : Con( Δ , Γ ) | and |A : Ty Γ| we construct |A[f ◁ g] = S ◁ P
: Ty Γ|. We obtain
|S| as the pullback of |f| and |P-A-s|. Given |s : S| , |P s| is the pushout of
|g s| and |P-A-g s| (this only type-checks after transporting along the
equations). 

\section*{Discussion}
\label{sec:discussion}

For our application we need to construct the model wrt to an already
constructed category of algebras instead of |Set|. Hence we need to
verify that pushouts exists. We need a constructive variant of locally
presentable categories here, which have the required colimits.

For our application we only need a basic CwF structure but we can also
interpret |Σ|-types and we expect that we can interpret |Π|-types in our model, the latter giving rise to higher-order abstract syntax.

One issue with our construction is that types and contexts are not
h-sets hence we need to address the coherence issues. We believe that
this fits very well with a generalisation of CwFs where types can be
groupoids (or 1-types) which we are also investigating (coherent
CwFs). 

%------------------------------------------------------------------------------

\newpage

\label{sect:bib}
\bibliographystyle{plain}
%\bibliographystyle{alpha}
%\bibliographystyle{unsrt}
%\bibliographystyle{abbrv}
\bibliography{references}

%------------------------------------------------------------------------------
\end{document}

