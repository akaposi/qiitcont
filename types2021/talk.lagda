\documentclass[handout]{beamer}

%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

\usepackage{tikz}
\usetikzlibrary{cd}

\author{
Thorsten Altenkirch
\and
Ambrus Kaposi}

\title{A container model of type theory}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Inductive types and container}

  An inductive type is the initial algebra |T : Set → Set| of a
  functor.
  
  E.g. |T X = 1 + X + (ℕ → X)|.

  \medskip
  
  Not all functors have initial algebras

  E.g. |R X = (X → 2) → 2|

  \medskip
  
  A container (aka polynomial functor) = strictly positive functor.

  |S : Set| \qquad shapes

  |P : S → Set| \qquad positions
  
  \begin{code}
    S ◁ P : Set → Set
    (S ◁ P) X = Σ s : S . P s → X
  \end{code}

  E.g.
  \begin{code}
    S = 3
    P 0 = 0
    P 1 = 1
    P 2 = ℕ
  \end{code}
  
\end{frame}

\begin{frame}
  \frametitle{Inductive-Inductive Types}

  \begin{code}
data Con : Set
data Ty : Con → Set

_,_ : (Γ : Con) → Ty Γ → Con
Π : (Γ : Con)(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
\end{code}

Cannot be understood as the initial algebra of a functor.

We [2] define a sequence of categories of algebras |A₀ , A₁ , A₂, ... |

Every constructor is given by functors

|Lᵢ : Aᵢ → Set| \qquad left hand side

|Rᵢ : ∫ Lᵢ → Set| \quad right hand side (|∫ Lᵢ = Σ X : Aᵢ . Lᵢ X|)

\begin{code}
Aᵢ₊₁ =
Σ  X : Aᵢ .
    cᵢ : (x : Lᵢ X) → R (X , x)   
\end{code}

\begin{code}
A₁ = Σ C:Set,T:C → Set, c₀ : (G:C) → T G → C

L₁ (C , T , c₀) = Σ G : C, A : T C, B : T (c₀ G A)

R₁ (C , T, c₀, G , A , B) = T G
\end{code}
  
\end{frame}

\begin{frame}
  \frametitle{Containerification}

  We want to restrict the left hand side |Lᵢ| to strictly positive
  types.

  Hence we need |L : A → Set| to be a container over |A| (|Cont A|).
  
  |S : Set| 

  |P : S → A| 
  
\begin{code}
S ◁ P : A → Set
(S ◁ P) X = Σ s : S . A( P s , X )
\end{code}

To be able to interpret the syntax we need that container form a
model of type theory (CwF - category with families).

\begin{code}
Con : Cat
Ty : Con → Set
Tm : ∫ Ty → Set
\end{code}

  For simplicity we consider the case |A = Set|. 
\end{frame}

\begin{frame}
  \frametitle{The model}

  \begin{code}

    Con : Set₁
    Con = Cont Set 

    Con( S ◁ P , T ◁ Q)
    = Σ f : S → T . (s : S) → Q (f s) → P s

    Ty : Con → Set₁
    Ty Γ = Cont (∫ Γ)

    Tm : ∫ Ty → Set
    Tm (Γ , A)
    =  ∫X (x : Γ X) → A (X , x)
    =  Σ  f : S-Γ → S-A .
            g : (s : S-A) → ∫ Γ (P-A s , (P-Γ (P-A-s s) , P-A-s s , id))
  \end{code}
 \end{frame}

 \begin{frame}
   \frametitle{The tricky bit}

   Type substitution. 

   \begin{code}
     A : Ty Δ
     δ : Cont(Γ , Δ)

     A [ δ  ] : Ty Γ
   \end{code}
   
   \textbf{Hint:} You need a pullback and a pushout.

   For details see our Agda formalisation. 
 \end{frame}

\begin{frame}
  
 \includegraphics[height=\textheight]{ty-subst.pdf}   
\end{frame}

 
 \begin{frame}
   \frametitle{Discussion}

   \begin{itemize}
   \item Different to Tamara von Glenn's model [7], see also Atkey [4]
     and Kovacs [6]:
   \begin{code}
    Ty : Con → Set₁
    Ty (S ◁ P) = Cont (∫ (Const S))
  \end{code}

\item The container model is not a strict CwF (unlike presheaves),
  need a notion of coherent CwF.
  
\item Need to complete formalisation of the container model and
  present it for arbitrary cats with colimits.

\item Details of the container model of QIITs need to be worked out.\\
  How much to steal from Adamek? (locally presentable cats). 

\item Other applications?
  
    \end{itemize}
   
 \end{frame}


 
\end{document}
