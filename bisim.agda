{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Agda.Builtin.Unit
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels
open import Cubical.Data.Empty
open import Cubical.Data.Unit
open import Cubical.Data.Nat
open import Cubical.Data.Sigma

{-
record isBisim {A B : Type}(R : A → B → Type) : Type where
  coinductive
  field
    coe→ : (a : A) → Σ B (R a)
    coe← : (b : B) → Σ A λ a → R a b
    bis  : {a a' : A}{b b' : B}(r : R a b)(r' : R a' b') →
           isBisim {A = a ≡ a'}{B = b ≡ b'}
             (λ p q → PathP (λ i → R (p i) (q i)) r r')
open isBisim



_~_ : Type → Type → Type₁
A ~ B = Σ (A → B → Type) isBisim

record _~'_ (A B : Type) : Type where
  coinductive
  field
    coe→ : A → B
    coe← : B → A
    eq   : {a : A}{b : B} → (coe→ a ≡ b) ~' (a ≡ coe← b)
open _~'_

{-# TERMINATING #-}
refl~ : (A : Type) → A ~' A
coe→ (refl~ A) = λ x → x
coe← (refl~ A) = λ x → x
eq   (refl~ A) {a}{b} = refl~ (a ≡ b)

{-# TERMINATING #-}
trans~ : {A B C : Type} → A ~' B → B ~' C → A ~' C
coe→ (trans~ e e') = λ a → coe→ e' (coe→ e a)
coe← (trans~ e e') = λ c → coe← e (coe← e' c)
eq (trans~ {A}{B}{C} e e'){a}{c} = trans~ {B = coe→ e a ≡ coe← e' c}
  (eq e' {coe→ e a}{c})
  (eq e {a}{coe← e' c})

{-# TERMINATING #-}
sym~ : {A B : Type} → A ~' B → B ~' A
coe→ (sym~ {A} {B} e) = coe← e 
coe← (sym~ {A} {B} e) = coe→ e
eq (sym~ {A} {B} e){b}{a} = sym~ (transport {!!} (eq e {a}{b}))

-- (trans~ refl~ e ≡ e)

{-
   ((coe→ a ≡ b) ~' (a ≡ coe← b)) ≡ ((coe→ a' ≡ b') ~' (a' ≡ coe← b'))


    eq*  : {a a' : A}(a= : a ≡ a')
           {b b' : B}(b= : b ≡ b')

           a'    b
           |     |
           a     b'

           (coe→ a= ≡ b=) ~' (PathP () a= ap coe← b=)

-}

→← : {A B : Type}(e : A ~' B) → {b : B} → coe→ e (coe← e b) ≡ b
→← e {b} = coe← (eq e {coe← e b}{b}) refl

f : {A B : Type} → A ~ B → A ~' B
coe→ (f {A} {B} (R , bisR)) = λ a → fst (coe→ bisR a)
coe← (f {A} {B} (R , bisR)) = λ b → fst (coe← bisR b)
eq   (f {A} {B} (R , bisR)) = {!!}

g : {A B : Type} → A ~' B → A ~ B
g e = (λ a b → b ≡ coe→ e a) , {!!}
H : {A B : Type}(e : A ~' B) {a : A}{b : B} → isBisim (λ a b → b ≡ coe→ e a)
coe→ (H e) = λ a → coe→ e a , refl
coe← (H e) = λ b → coe← e b , sym (→← e {b})
bis  (H e) {a}{a'}{b}{b'} r r' = {!H !}
-- PathP () p q

{-
a : U
x, y : El a
Id_{El a} x y =

Id_{El (cΣ a b)} x y = Id_Σ (El a) (El b) x y


a : Tm (Γ++Ω) U
ω ψ : Tms Γ Ω
ρ : Tms Γ (Ids ω ψ)
u : Tm Γ (El a[ω])
v : Tm Γ (El a[ψ])
---------------------------------------
Idd_{El a}


p : Id_U a b
u : El a
v : El b
----------------------------------------
Idd_{El x} p u v = (coe→ p u =_{El b} v)
                 = (u =_{El a} coe← p v)

coh := refl : Idd_{El a} ρ u (coe→ u) = Id (coe→ u) (coe→ u)


ap cΣ : (p:a =_U a')(q : Idd (A→U) {A:=a,a'} p b b') → cΣ a b =_U cΣ a' b'
                     ^ = b =_p b'
                       = (x:a)(x':a')(x~:Idd (El a) p x x')→ (b x) =_U (b x')

coe→ : cΣ a b → cΣ a' b'
coe→ (cΣ a b) (x,y) := (coe→ p x , coe→ (q x (coe→ p x) refl) y)
coe← ...

(coe→ (x,y) = (x',y')) =_U ((x,y) = coe← (x',y'))
( (coe→ p x , coe→ (q x (coe→ p x) refl) y) =_{El cΣ a' b'} (x',y'))  =_U   ((x,y) =_{El cΣ a b} (coe← p x' , coe← (q _ _ refl) y' ) )

( Σ (x~ : coe→ p x =_{a'} x') (Idd (b' x) (x~) (coe→ (q x (coe→ p x) refl) y) y') )   =_U   (Σ (x=coe← p x') (...))


eq p  : (coe→ p x =_{El a'} x') =_U (x =_{El a} coe← p x')
eq (q ? ? ) :              (coe→ (q x (coe→ p x) refl) y = y')      =_U   (y = coe← (q (coe← p x') x refl) y')
eq (q x (coe→ p x) refl) : (coe→ (q x (coe→ p x) refl) y = y')      =_U   (y = coe← (q x (coe→ p x) refl) y')
eq (q x (coe→ p x) refl) : (coe→ (q (coe← p x') x refl) y = y')     =_U   (y = coe← (q (coe← p x') x refl) y')


(y = coe← (q x (coe→ p x) refl) y')         (coe→ (q (coe← p x') x refl) y = y')
-}

-}

{-
record _~_ (A B : Type) : Type₁ where
  field
     coe→ : A → B
     coh→ : (a : A) → isContr (R a (coe→ a))
     coe← : B → A
     coh← : (b : B) → isContr (R (coe← b) b)
-}

{-
record isBisim {A B : Type}(R : A → B → Type) : Type where
  coinductive
  field
     coe→ : A → B
     coh→ : (a : A) → R a (coe→ a)
     coe← : B → A
     coh← : (b : B) → R (coe← b) b
     bis  : {a a' : A}{b b' : B}(r : R a b)(r' : R a' b') →
           isBisim {A = a ≡ a'}{B = b ≡ b'}
             (λ p q → PathP (λ i → R (p i) (q i)) r r')
-- isBisim (λ p q → Id^{p,q}_{x y.R x y} r r')  
open isBisim
-}

record isBisim {A₀ A₁ : Type}(R : A₀ → A₁ → Type) : Type where
  coinductive
  field
     coe→ : A₀ → A₁
     coh→ : (a₀ : A₀) → R a₀ (coe→ a₀)
     coe← : A₁ → A₀
     coh← : (a₁ : A₁) → R (coe← a₁) a₁ 
     bis  : {a₀₀ a₁₀ : A₀}{a₀₁ a₁₁ : A₁}(r₀ : R a₀₀ a₀₁)(r₁ : R a₁₀ a₁₁) →
           isBisim {A₀ = a₀₀ ≡ a₁₀}{A₁ = a₀₁ ≡ a₁₁}
             (λ p₀ p₁ → PathP (λ i → R (p₀ i) (p₁ i)) r₀ r₁)
-- isBisim (λ p q → Id^{p,q}_{x y.R x y} r r')  
open isBisim

{-
R is equivalence
then 
 A --R-- B
 ||      ||
 ||      ||
 A --R-- B

If R is an equivalence between A and B
then for any a,a':A, b,b':B
the relation 
R^= : (a ≡ a') → (b ≡ b') is an equivalence.

A,B : U
p : A ≡ B 
Id^{p} : El A → El B → U
El(p) = p.1

f : (x : A) → B x
p : a ≡ b
-------------------------
ap f : Id^{p} (f a) (f b) 

(R , bR) : A ≡ A'
(S , bS) : B ≡ B'
-------------------------------------
(R ×R S , bR ×b bS) : A × B ≡ A' × B'
(ap ×)

-}

_~_ : Type → Type → Type₁
A ~ B = Σ (A → B → Type) isBisim

{-
ref : (A : Type) → isBisim (_≡_ {A = A})
coe→ (ref A) x = x
coh→ (ref A) x = refl
coe← (ref A) x = x
coh← (ref A) x = refl
bis  (ref A) {a}{a'}{b}{b'} r r' = {!bis !}
-}

_×R_ : {A B A' B' : Type}
       (R : A → A' → Type)
       (S : B → B' → Type)
       → (A × B) → (A' × B') → Type
-- (R ×R S) (a , b) (a' , b') = R a a' × S b b'
(R ×R S) p q = R (fst p) (fst q) × S (snd p) (snd q)

{-
module _
  {A B A' B' : Type}{R : A → A' → Type}{S : B → B' → Type}{a a' : A}{b b' : B}{c c' : A'}{d d' : B'}
  {r : R a c}{r' : R a' c'}{s : S b d}{s' : S b' d'}
  where
  ×eq'' :
    (λ (p : (a , b) ≡ (a' , b'))(q : (c , d) ≡ (c' , d')) → PathP (λ i → R (fst (p i)) (fst (q i)) × (S (snd (p i)) (snd (q i)))) (r , s) (r' , s')) ≡
    (λ (p : (a , b) ≡ (a' , b'))(q : (c , d) ≡ (c' , d')) → PathP (λ i → R (fst (p i)) (fst (q i))) r r' × PathP (λ i → S (snd (p i)) (snd (q i))) s s')
    -- (λ p q → (PathP (λ i → R (fst p i) (fst q i)) r r') × (PathP (λ i → S (snd p i) (snd q i)) s s'))
  ×eq'' i p q = sym (ΣPath≡PathΣ {A = λ i → R (fst (p i)) (fst (q i))}{λ i _ → S (snd (p i)) (snd (q i))}{r , s}{r' , s'}) i

  ×eq' :
    isBisim {(a , b) ≡ (a' , b')}{(c , d) ≡ (c' , d')}(λ p q → PathP (λ i → R (fst (p i)) (fst (q i))) r r' × PathP (λ i → S (snd (p i)) (snd (q i))) s s') ≡
    isBisim {(a ≡ a') × (b ≡ b')}{(c ≡ c') × (d ≡ d')}(λ p q → PathP (λ i → R (fst  p i ) (fst  q i )) r r' × PathP (λ i → S (snd  p i ) (snd  q i) ) s s')
  ×eq' j = isBisim {ΣPath≡PathΣ {A = λ _ → A}{λ _ _ → B}{a , b}{a' , b'} (~ j)}{ΣPath≡PathΣ {A = λ _ → A'}{λ _ _ → B'}{c , d}{c' , d'} (~ j)}
    (λ p q → PathP (λ i → R {!p!} {!!}) r r' × PathP (λ i → S {!!} {!!}) s s')

  ×eq :
    isBisim {(a ≡ a') × (b ≡ b')}{(c ≡ c') × (d ≡ d')}(λ p q → PathP (λ i → R (fst  p i ) (fst  q i )) r r' × PathP (λ i → S (snd  p i ) (snd  q i) ) s s') ≡
    isBisim {(a , b) ≡ (a' , b')}{(c , d) ≡ (c' , d')}(λ p q → PathP (λ i → R (fst (p i)) (fst (q i)) × (S (snd (p i)) (snd (q i)))) (r , s) (r' , s'))
  ×eq = sym ×eq' ∙ λ i → isBisim {(a , b) ≡ (a' , b')}{(c , d) ≡ (c' , d')} (×eq'' (~ i))

  ×= : {A B : I → Type}{u : A i0}{u' : A i1}{v : B i0}{v' : B i1} → PathP (λ i → A i × B i) (u , v) (u' , v') ≡ (PathP A u u' × PathP B v v')
  ×= {A}{B}{u}{u'}{v}{v'} = sym ΣPath≡PathΣ

{-# TERMINATING #-}
_×b_  : {A B A' B' : Type}
        {R : A → A' → Type}
        (bR : isBisim R)
        {S : B → B' → Type}
        (bS : isBisim S)
        → isBisim (R ×R S)
coe→ (bR ×b bS) (a , b) = (coe→ bR a) , (coe→ bS b)
coh→ (bR ×b bS) (a , b) = (coh→ bR a) , (coh→ bS b)
coe← (bR ×b bS) (a , b) = (coe← bR a) , (coe← bS b)
coh← (bR ×b bS) (a , b) = (coh← bR a) , (coh← bS b)
bis (bR ×b bS) {a = (a , b)} {a' = (a' , b')}{b = (c , d)}{b' = (c' , d')} (r , s) (r' , s') = transport (×eq {a = a}{a'}{b}{b'}{c}{c'}{d}{d'}{r}{r'}{s}{s'}) (bis bR r r' ×b bis bS s s')
-}
{-
isBism (λ p q → PathP (λ i → (R ×R S) (p i) (q i)) (r , s) (r' , s'))
= -- η-exp (p i) (q i)
isBism (λ {p₀ , p₁} {q₀ , q₁} → PathP (λ i → (R ×R S) (p₀ i , p₁ i) (q₀ i , q₁ i))) (r , s) (r' , s'))
= -- defn of ×R
isBism (λ {p₀ , p₁} {q₀ , q₁} → PathP (λ i → (R (p₀ i) (q₀ i)) × (S (p₁ i) (q₁ i))) (r , s) (r' , s'))
= -- PathP prop ???
isBism (λ {p₀ , p₁} {q₀ , q₁} → (PathP (λ i → R (p₀ i) (q₀ i)) r r') × (PathP (λ i → S (p₁ i) (q₁ i) s s'))
= -- defn of ×R
isBism (λ p₀ q₀ → PathP (λ i → R (p₀ i) (q₀ i)) r r') ×R (λ p₁ q₁ → PathP (λ i → S (p₁ i) (q₁ i)) s s')    
= _×b_
isBism (λ p₀ q₀ → PathP (λ i → R (p₀ i) (q₀ i)) r r') × isBisim (λ p₁ q₁ → PathP (λ i → S (p₁ i) (q₁ i)) s s')    

Id^ρ_{Δ.A × B} (u₀ , u₁) (v₀ , v₁) = Id^ρ (Δ.A u₀ v₀) × Id^ρ (Δ.B u₁ v₁)

???
Id^{ρ₀ , ρ₁}_{Δ₀ × Δ₁.A × B} (u₀ , u₁) (v₀ , v₁) = Id^ρ₀_{Δ₀.A} u₀ v₀ × Id^ρ₁_{Δ₁.B} u₁ v₁

rs = (r , s) : (R a₀ a₁) × (S b₀ b₁) 
rs' = (r' , s') : (R a₀' a₁') × (S b₀' b₁') 

isBisim (λ p q → Id^{p,q}_{x y.(R ×R S) x y} rs rs')  
= η exp
isBisim (λ {p₀ , p₁} {q₀ , q₁} → Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , a₁),(b₀ , b₁).(R a₀ b₀) × (S a₁ b₁)} (r , s) (r' , s')
= def of Id for ×
isBisim (λ {p₀ , p₁} {q₀ , q₁} → (Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , a₁),(b₀ , b₁).(R a₀ b₀)} r r')
                               × (Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , a₁),(b₀ , b₁).(S a₁ b₁)} s s'))
= curry
isBisim (λ {p₀ , p₁} {q₀ , q₁} → (Id^{p₀,p₁,q₀,q₁}_{a₀,a₁,b₀,b₁).(R a₀ b₀)} r r')
                               × (Id^{p₀,p₁,q₀,q₁}_{a₀,a₁,b₀,b₁).(S a₁ b₁)} s s'))
= non-dep
isBisim (λ {p₀ , p₁} {q₀ , q₁} → (Id^{p₀,q₀}_{(a₀,b₀).(R a₀ b₀)} r r')
                               × (Id^{p₁,q₁}_{(a₁,b₁).(S a₁ b₁)} s s'))
= def of ×R
isBisim ((λ p₀ q₀ → (Id^{(p₀ , q₀)}_{(a₀,b₀).(R a₀ b₀)} r r')) ×R (λ p₁ q₁ → Id^{(p₁ , q₁)}_{(a₁,b₁).(S a₁ b₁)} s s'))
= coind hyp
(isBisim (λ p₀ q₀ → (Id^{(p₀ , q₀)}_{(a₀,b₀).(R a₀ b₀)} r r')) × isBisim (λ p₁ q₁ → Id^{(p₁ , q₁)}_{(a₁,b₁).(S a₁ b₁)} s s'))
-- now use (bis bR , bis bS)
-}



ΣR : {A A' : Type}
     {B : A → Type}{B' : A' → Type}
     (R : A → A' → Type)
     (S : {a : A}{a' : A'} → R a a' → B a → B' a' → Type)
     → (Σ A B) → (Σ A' B') → Type
ΣR R S (a , b) (a' , b') = Σ (R a a') λ r → S r b b'


{-
Σb : {A A' : Type}
     {B : A → Type}{B' : A' → Type}
     {R : A → A' → Type}
     (bR : isBisim R)
     {S : {a : A}{a' : A'} → R a a' → B a → B' a' → Type}
     (bS : {a : A}{a' : A'}(r : R a a') → isBisim (S r))
     → isBisim (ΣR R S)
coe→ (Σb bR bS) (a , b) = coe→ bR a , coe→ (bS (coh→ bR a)) b
coh→ (Σb bR bS) (a , b) = (coh→ bR a) , (coh→ (bS (coh→ bR a)) b)
coe← (Σb bR bS) (a' , b') = coe← bR a' , coe← (bS (coh← bR a')) b'
coh← (Σb bR bS) (a' , b') = (coh← bR a') , (coh← (bS (coh← bR a')) b')
bis (Σb bR bS) {a = (a₀ , b₀)}{a' = (a₀' , b₀')}{b = (a₁ , b₁)}{b' = (a₁' , b₁')} (r , s)  (r' , s') = {!!}
--bis (Σb bR bS) {a = (a₀ , b₀)}{b = (a₁ , b₁)}{a' = (a₀' , b₀')}{b' = (a₁' , b₁')} r r' = {!!}
-}
     
{- 
Goal: isBisim
      (λ p q → PathP (λ i → ΣR R S (p i) (q i)) (r , s) (r' , s'))
————————————————————————————————————————————————————————————
s'  : S r' b₀' b₁'
r'  : R a₀' a₁'
s   : S r b₀ b₁
r   : R a₀ a₁

p : (a₀ , b₀) ≡ (a₀' , b₀')
q : (a₁ , b₁) ≡ (a₁' , b₁')

p₀ : a₀ ≡ a₀'
p₁ : Id_B^{p₀} b₀ b₀'
q₀ : a₁ ≡ a₁'
q₁ : Id_B^{q₀} b₁ b₁'

isBisim (λ {p₀ , p₁} {q₀ , q₁} → Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , b₀),(a₁ , b₁)}_{Σ (R a₀ a₁) λ r → S r b₀ b₁} (r , r) (r' , s')
=
isBisim (λ {p₀ , p₁} {q₀ , q₁} → Σ (Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , b₀),(a₁ , b₁)}_{R a₀ a₁} r r')
                                 (λ rr → Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , b₀),(a₁ , b₁),rr}_{S r b₀ b₁} s s')
=
isBisim (λ {p₀ , p₁} {q₀ , q₁} → Σ (Id^{p₀,q₀}_{(a₀,a₁}_{R a₀ a₁} r r')
                                 (λ rr → Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , b₀),(a₁ , b₁),rr}_{S r b₀ b₁} s s')
=
isBisim (ΣR (Id^{p₀,q₀}_{(a₀,a₁}_{R a₀ a₁} r r')  (λ rr → Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , b₀),(a₁ , b₁),rr}_{S r b₀ b₁} s s')
=
RR r r' = (Id^{p₀,q₀}_{(a₀,a₁}_{R a₀ a₁} r r'
SS rr s s' = Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , b₀),(a₁ , b₁)} (Id^{rr}(S r b₀ b₁) s s') 
=




??
isBisim (ΣR (Id^{p₀,q₀}_{(a₀,a₁}_{R a₀ a₁} r r')
× Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , b₀),(a₁ , b₁), rr)(S r b₀ b₁) s s'

=
(λ p₀ q₀ → (Id^{(p₀ , q₀)}_{(a₀,b₀).(R a₀ b₀)} r r')) × 
{a₀ a₁ : A}(r : R a₀ a₁) → Id^{(p₀ , p₁),(q₀ , q₁)}_{(a₀ , b₀),(a₁ , b₁)}_{S r b₀ b₁} s s'
=
(isBisim R) × {a₀ a₁ : A}(r : R a₀ a₁) → isBisim (S r) 

can't we use ap here?
we know that Id preserves equality hence we can reason about 
Id r r'
-}

record isBisimD {A₀ A₁ : Type}(R : A₀ → A₁ → Type)
                {B₀ : A₀ → Type}{B₁ : A₁ → Type}
                (S : {a₀ : A₀}{a₁ : A₁} → R a₀ a₁ → B₀ a₀ → B₁ a₁ → Type)  : Type where
  coinductive
  field
     coe→ : {a₀ : A₀}{a₁ : A₁} → R a₀ a₁ → B₀ a₀ → B₁ a₁
     coh→ : {a₀ : A₀}{a₁ : A₁}(r : R a₀ a₁)(b₀ : B₀ a₀) → S r b₀ (coe→ r b₀)
     coe← : {a₀ : A₀}{a₁ : A₁} → R a₀ a₁ → B₁ a₁ → B₀ a₀ 
     coh← : {a₀ : A₀}{a₁ : A₁}(r : R a₀ a₁)(b₁ : B₁ a₁) → S r (coe← r b₁) b₁
     bis  : {a₀₀ a₁₀ : A₀}{a₀₁ a₁₁ : A₁}
            (r₀ : R a₀₀ a₀₁)(r₁ : R a₁₀ a₁₁)
            {b₀₀ : B₀ a₀₀}{b₁₀ : B₀ a₁₀}{b₀₁ : B₁ a₀₁}{b₁₁ : B₁ a₁₁}
            (s₀ : S r₀ b₀₀ b₀₁)(s₁ : S r₁ b₁₀ b₁₁)
            → let A₀' = a₀₀ ≡ a₁₀
                  A₁' = a₀₁ ≡ a₁₁
                  R' : A₀' → A₁' → Type
                  R' p₀ p₁ = PathP (λ i → R (p₀ i) (p₁ i)) r₀ r₁
                  B₀' : A₀' → Type
                  B₀' p₀ = PathP (λ i → B₀ (p₀ i)) b₀₀ b₁₀
                  B₁' : A₁' → Type
                  B₁' p₁ = PathP (λ i → B₁ (p₁ i)) b₀₁ b₁₁
                  S' : {p₀ : A₀'}{p₁ : A₁'} → R' p₀ p₁ → B₀' p₀ → B₁' p₁ → Type
                  S' {p₀} {p₁} rr q₀ q₁ = PathP (λ i → S (rr i) (q₀ i) (q₁ i)) s₀ s₁
              in isBisimD {A₀ = A₀'}{A₁ = A₁'} R' {B₀'} {B₁'} S'

open isBisimD

Σb : {A A' : Type}
     {B : A → Type}{B' : A' → Type}
     {R : A → A' → Type}
     (bR : isBisim R)
     {S : {a : A}{a' : A'} → R a a' → B a → B' a' → Type}
     (bS : isBisimD R S)
     → isBisim (ΣR R S)
coe→ (Σb bR bS) (a , b) = (coe→ bR a) , (coe→ bS (coh→ bR a) b)
coh→ (Σb bR bS) (a , b) = (coh→ bR a) , (coh→ bS (coh→ bR a) b)
coe← (Σb bR bS) (a , b) = (coe← bR a) , (coe← bS (coh← bR a) b)
coh← (Σb bR bS) (a , b) = (coh← bR a) , (coh← bS (coh← bR a) b)
bis (Σb bR bS) {a₀₀ = (a₀₀ , b₀₀)}{a₁₀ = (a₁₀ , b₁₀)}{a₀₁ = (a₀₁ , b₀₁)}{a₁₁ = (a₁₁ , b₁₁)} (r₀ , s₀)  (r₁ , s₁) = {!!}
{-
Goal: isBisim
      (λ p₀ p₁ → PathP (λ i → ΣR R S (p₀ i) (p₁ i)) (r₀ , s₀) (r₁ , s₁))
————————————————————————————————————————————————————————————
s₁  : S r₁ b₁₀ b₁₁
r₁  : R a₁₀ a₁₁
s₀  : S r₀ b₀₀ b₀₁
r₀  : R a₀₀ a₀₁
b₁₁ : B' a₁₁
a₁₁ : A'
b₀₁ : B' a₀₁
a₀₁ : A'
b₁₀ : B a₁₀
a₁₀ : A
b₀₀ : B a₀₀
a₀₀ : A
bS  : isBisimD R S
S   : {a : A} {a' : A'} →
      R a a' → B a → B' a' → Type   (not in scope)
bR  : isBisim R

isBisim (λ (p₀ , q₀) (p₁ , q₁) → Id^{(p₀ , q₀),(p₁ , q₁)}_{(a₀ᵢ , b₀ᵢ),(a₁ᵢ , b₁ᵢ)}_{Σ (R a₀ᵢ a₁ᵢ) (λ rᵢ → S rᵢ b₀ᵢ b₁ᵢ)} (r₀ , s₀) (r₁ , s₁)
=
isBisim (λ (p₀ , q₀) (p₁ , q₁) → Σ Id^{p₀,p₁}_{a₀ᵢ,a₁ᵢ . R a₀ᵢ a₁ᵢ} r₀ r₁
                                 λ rr →  Id^{p₀,q₀,p₁,q₁}_{a₀ᵢ,b₀ᵢ,a₁ᵢ,b₁ᵢ,rᵢ.S rᵢ b₀ᵢ b₁ᵢ} s₀ s₁
=
isBisim (ΣR (λ p₀ p₁ → Id^{p₀,p₁}_{a₀ᵢ,a₁ᵢ . R a₀ᵢ a₁ᵢ} r₀ r₁) 
            (λ (p₀ , q₀) (p₁ , q₁) rr → Id^{p₀,q₀,p₁,q₁}_{a₀ᵢ,b₀ᵢ,a₁ᵢ,b₁ᵢ,rᵢ.S rᵢ b₀ᵢ b₁ᵢ} s₀ s₁)
=
isBisim (λ p₀ p₁ → Id^{p₀,p₁}_{a₀ᵢ,a₁ᵢ . R a₀ᵢ a₁ᵢ} r₀ r₁)
× isBisimD (λ p₀ q₀ p₁ q₁ rr → Id^{p₀,q₀,p₁,q₁}_{a₀ᵢ,b₀ᵢ,a₁ᵢ,b₁ᵢ,rᵢ.S rᵢ b₀ᵢ b₁ᵢ} s₀ s₁)

(bis bR , bis bS)
-}

_→R_ : {A₀ B₀ A₁ B₁ : Type}
       (R : A₀ → A₁ → Type)
       (S : B₀ → B₁ → Type)
       → (A₀ → B₀) → (A₁ → B₁) → Type
(R →R S) f₀ f₁ = ∀ a₀ a₁ → R a₀ a₁ → S (f₀ a₀) (f₁ a₁)

_→b_  : {A₀ B₀ A₁ B₁ : Type}
        {R : A₀ → A₁ → Type}
        (bR : isBisim R)
        {S : B₀ → B₁ → Type}
        (bS : isBisim S)
        → isBisim (R →R S)
coe→ (_→b_ {A₀} {B₀} {A₁} {B₁} {R} bR {S} bS) f₀ a₁ = coe→ bS (f₀ (coe← bR a₁))
coh→ (_→b_ {A₀} {B₀} {A₁} {B₁} {R} bR {S} bS) f₀ a₀ a₁ r =
  let q : a₀ ≡ coe← bR a₁
      q = coe← (bis bR r (coh← bR a₁)) refl
      p : S (f₀ (coe← bR a₁)) (coe→ bS (f₀ (coe← bR a₁)))
      p = coh→ bS (f₀ (coe← bR a₁))
      b₁ : B₁
      b₁ = coe→ bS (f₀ (coe← bR a₁))
  in subst (λ b₀ → S b₀ b₁) (cong f₀ (sym q)) p
coe← (_→b_ {A₀} {B₀} {A₁} {B₁} {R} bR {S} bS) = {!!}
coh← (_→b_ {A₀} {B₀} {A₁} {B₁} {R} bR {S} bS) = {!!}
bis (_→b_ {A₀} {B₀} {A₁} {B₁} {R} bR {S} bS) = {!!}

{-
coe→ (bR →b bS) f₀ = λ a₁ → coe→ bS (f₀ (coe← bR a₁))
coh→ (bR →b bS) f₀ r = {!!}
coe← (bR →b bS) = {!!}
coh← (bR →b bS) = {!!}
bis (bR →b bS) = {!!}
-}
{-
Goal: S (f₀ a₀) (coe→ bS (f₀ (coe← bR a₁)))
Have: R a₁ (coe→ bR a₁)

-}
