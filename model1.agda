{-# OPTIONS --cubical #-}
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Agda.Builtin.Unit
open import Cubical.Data.Empty

-- this file defines types as left Kan extensions to support substitution

infixl 5 _▹_
infixl 7 _[_]T
infixl 5 _,s_
infixr 6 _∘_
infixl 8 _[_]t

record Con : Set₁ where
  field
    S : Set
    P : S → Set
open Con

record Sub (Γ Δ : Con) : Set where
  field
    S : S Γ → S Δ
    P : {sΓ : Con.S Γ} → P Δ (S sΓ) → P Γ sΓ
open Sub

_∘_ : ∀{Γ Θ Δ} → Sub Θ Δ → Sub Γ Θ → Sub Γ Δ
S (σ ∘ δ) = λ sΓ → S σ (S δ sΓ)
P (σ ∘ δ) = λ pΔ → P δ (P σ pΔ)

id : ∀{Γ} → Sub Γ Γ
S id = λ sΓ → sΓ
P id = λ pΓ → pΓ

∙ : Con
S ∙ = ⊤
P ∙ * = ⊥

ε : ∀{Γ} → Sub Γ ∙
S ε _ = tt
P ε ()

record Ty' (C : Con) : Set₁ where
  field
    S : S C → Set
    P : {sC : Con.S C} → S sC → Set
    Q : {sC : Con.S C}(sA : S sC) → Con.P C sC → P sA
open Ty'

-- maybe Ty' is not a functor, but a bifunctor, and Ty is not a colimit, but a coend

record Ty (Γ : Con) : Set₁ where
  field
    C : Con
    s : Sub Γ C
    T : Ty' C
open Ty

-- or we can quotient by saying that if the presheaf extension of two
-- types are the same, then the two types are the same

_[_]T : ∀{Δ}(A : Ty Δ){Γ}(σ : Sub Γ Δ) → Ty Γ
C (A [ σ ]T) = C A
s (A [ σ ]T) = s A ∘ σ
T (A [ σ ]T) = T A

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    S : (sΓ : S Γ) → S (T A) (S (s A) sΓ)
    P : {sΓ : Con.S Γ} → P (T A) (S sΓ) → P Γ sΓ
    Q : {sΓ : Con.S Γ}(x : Con.P (C A) (Sub.S (s A) sΓ)) →
         Sub.P (s A) x ≡ P (Q (T A) (S sΓ) x)
open Tm
{-                Q (T A) (S sΓ)
P (C A) (S (s A) sΓ)---------->P (T A) (S sΓ)
                  \            /
                   \          /
     Sub.P (s A) sΓ \        /P t
                     \      /
                      v    v
                      P Γ sΓ                    -}

_[_]t : ∀{Δ}{A : Ty Δ}(t : Tm Δ A){Γ}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
Tm.S (t [ σ ]t) = λ sΓ → S t (S σ sΓ)
Tm.P (t [ σ ]t) = λ pA → P σ (P t pA)
Tm.Q (t [ σ ]t) = λ x → cong (P σ) (Q t x)

-- pushout
data PO {A B C : Set}(f : C → A)(g : C → B) : Set where
  inl : A → PO f g
  inr : B → PO f g
  glu : (c : C) → inl (f c) ≡ inr (g c)

_▹_ : (Γ : Con) → Ty Γ → Con
S (Γ ▹ A) = Σ (S Γ) λ sΓ → S (T A) (S (s A) sΓ)
P (Γ ▹ A) (sΓ , sA) = PO (P (s A)) (Q (T A) sA)

p : ∀{Γ A} → Sub (Γ ▹ A) Γ
S p = fst
P p = inl

q : ∀{Γ A} → Tm (Γ ▹ A) (A [ p {Γ}{A} ]T)
S (q {Γ} {A}) = snd
P (q {Γ} {A}) = inr
Q (q {Γ} {A}) = glu

_,s_ : ∀{Γ Δ A}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▹ A)
S (σ ,s t) sΓ = S σ sΓ , S t sΓ
P (σ ,s t) (inl pΔ) = P σ pΔ
P (σ ,s t) (inr pA) = P t pA
P (σ ,s t) (glu c i) = Q t c i

-- equalities

-- Sub= : ∀{Γ Δ}{σ₀ σ₁ : Sub Γ Δ}(S= : S σ₀ ≡ S σ₁)(P= : _≡_ {A = {sΓ : Con.S Γ} → P Δ (S σ₁ sΓ) → P Γ sΓ} (subst (λ z → {sΓ : Con.S Γ} → P Δ (z sΓ) → P Γ sΓ) S= (P σ₀)) (P σ₁)) → σ₀ ≡ σ₁
-- Sub= refl p = {!p!}

ass : ∀{Γ Θ Ω Δ}{σ : Sub Ω Δ}{δ : Sub Θ Ω}{ν : Sub Γ Θ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = refl

idl : ∀{Γ Δ}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
idl = refl

idr : ∀{Γ Δ}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
idr = refl

∙η : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε
∙η = {!!}

[∘]T : ∀{Γ Θ Δ}{A : Ty Δ}{σ : Sub Θ Δ}{δ : Sub Γ Θ} → A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T
[∘]T = refl

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T = refl

[∘]t : ∀{Γ Θ Δ}{A : Ty Δ}{t : Tm Δ A}{σ : Sub Θ Δ}{δ : Sub Γ Θ} → t [ σ ∘ δ ]t ≡ t [ σ ]t [ δ ]t
[∘]t = refl

[id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡ t
[id]t = refl

▹β₁ : ∀{Γ}{Δ}{A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → p {Δ}{A} ∘ (σ ,s t) ≡ σ
▹β₁ = refl

▹β₂ : ∀{Γ}{Δ}{A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → q {Δ}{A} [ σ ,s t ]t ≡ t
▹β₂ = refl

▹η : ∀{Γ}{Δ}{A}{σ : Sub Γ (Δ ▹ A)} → (p {Δ}{A} ∘ σ ,s q {Δ}{A} [ σ ]t) ≡ σ
▹η = {!!}
