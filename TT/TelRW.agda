{-# OPTIONS --rewriting #-}
{-
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels
-}
open import Relation.Binary.PropositionalEquality

module TT.TelRW where

{-# BUILTIN REWRITE _≡_ #-}

Type = Set

postulate
  Con : Type
  Sub : Con → Con → Type
  Ty  : Con → Type
  Tm  : (Γ : Con) → Ty Γ → Type
  Tys : Con → Type
  Tms : (Γ : Con) → Tys Γ → Type

variable
  Γ Δ Θ Ξ : Con
  γ δ ϑ : Sub Γ Δ
  A B C : Ty Γ
  a b c : Tm Γ A
  Ω Ψ Ζ : Tys Γ
  ω ψ ζ : Tms Γ Ω   

postulate
  -- category
  _∘_         : ∀{Γ Θ Δ} → Sub Θ Δ → Sub Γ Θ → Sub Γ Δ
  ass         : ∀{Γ Θ Ξ Δ}(σ : Sub Ξ Δ)(δ : Sub Θ Ξ)(ν : Sub Γ Θ) →
                (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  id          : ∀{Γ} → Sub Γ Γ
  idl         : ∀{Γ Δ}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
  idr         : ∀{Γ Δ}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
  {-# REWRITE ass idl idr #-}
  
  -- terminal object
  ◆           : Con
  ε           : ∀{Γ} → Sub Γ ◆
  ◆η          : ∀{Γ}(σ : Sub Γ ◆) → σ ≡ ε
  
  -- a family structure
  _[_]T       : ∀{Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
  _[_∣_]T     : ∀{Γ Θ Δ}(A : Ty Δ)(σ : Sub Θ Δ)(δ : Sub Γ Θ) →
                A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T
  _[id]T      : ∀{Γ}(A : Ty Γ) → A [ id ]T ≡ A
  {-# REWRITE _[_∣_]T _[id]T #-}
  _[_]t       : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
  _[_∣_]t     : a [ γ ]t [ δ ]t ≡ a [ γ ∘ δ ]t
  _[id]t      : a [ id ]t ≡ a
  {-# REWRITE _[_∣_]t _[id]t #-}

  -- extend a con with a ty
  _▹_         : (Γ : Con) → Ty Γ → Con
  _,s_        : ∀{Γ Δ A}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▹ A)
  π₁          : ∀{Γ Δ A}(σ : Sub Γ (Δ ▹ A)) → Sub Γ Δ
  π₂          : ∀{Γ Δ A}(σ : Sub Γ (Δ ▹ A)) → Tm Γ (A [ π₁ σ ]T)
  ▹β₁         : ∀{Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → π₁ (_,s_ {A = A} σ t) ≡ σ
  {-# REWRITE ▹β₁ #-}
  ▹β₂         : ∀{Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → π₂ (_,s_ {A = A} σ t) ≡ t
  ▹η          : ∀{Γ Δ A}{σ : Sub Γ (Δ ▹ A)} → π₁ σ ,s π₂ σ ≡ σ
  π₁∘         : ∀{Γ Δ A}{σ : Sub Γ (Δ ▹ A)}{Θ}{δ : Sub Θ Γ} → π₁ (σ ∘ δ) ≡ π₁ σ ∘ δ
  {-# REWRITE ▹β₂ ▹η π₁∘ #-}
  π₂[]        : ∀{Γ Δ A}{σ : Sub Γ (Δ ▹ A)}{Θ}{δ : Sub Θ Γ} → π₂ σ [ δ ]t ≡ π₂ (σ ∘ δ)
  _^_         : ∀{Γ Δ}(σ : Sub Γ Δ)(A : Ty Δ) → Sub (Γ ▹ A [ σ ]T) (Δ ▹ A)

  -- dependent family structure
  _[_]L       : Tys Δ → Sub Γ Δ → Tys Γ
  _[_]l       : Tms Δ Ω → (σ : Sub Γ Δ) → Tms Γ (Ω [ σ ]L)
  _[id]L      : (Ω : Tys Γ) → Ω [ id ]L ≡ Ω
  _[_∣_]L     : (Ω : Tys Γ) → Ω [ γ ]L [ δ ]L ≡ Ω [ γ ∘ δ ]L 
  -- TODO: functoriality
  {-# REWRITE _[id]L _[_∣_]L #-}
  
  -- terminal object in every slice (in depdent contexts = telescope = types)
  ◆L          : ∀{Γ} → Tys Γ
  εL          : ∀{Γ} → Tms Γ ◆L
  ◆Lη         : ∀{Γ}(σ : Tms Γ ◆L) → σ ≡ εL
  ◆[]         : ◆L [ γ ]L ≡ ◆L
  {-# REWRITE ◆[] #-}
  ε[]         : εL [ γ ]l ≡ εL

  -- extend a con with a tel
  _++L_       : (Γ : Con) → Tys Γ → Con
  idlL        : Γ ++L ◆L ≡ Γ
  {-# REWRITE idlL #-}
  -- id++l_      : Tms Γ Ω → Sub Γ (Γ ++L Ω)
  _++l_       : (σ : Sub Γ Δ) → Tms Γ (Ω [ σ ]L) → Sub Γ (Δ ++L Ω)
  π₁++        : Sub Γ (Δ ++L Ω) → Sub Γ Δ
  π₂++        : (σ : Sub Γ (Δ ++L Ω)) → Tms Γ (Ω [ π₁++ {Γ = Γ}{Ω = Ω} σ ]L)
  π₁++∘       : π₁++ {Γ = Γ}{Δ = Δ}{Ω = Ω} γ ∘ δ ≡ π₁++ {Γ = Θ}{Δ = Δ}{Ω = Ω}(γ ∘ δ)
  {-# REWRITE π₁++∘ #-}
  π₂++[]      : π₂++ {Γ = Γ}{Δ = Δ}{Ω = Ω} γ [ δ ]l ≡ π₂++ {Γ = Θ}{Δ = Δ}{Ω = Ω}(γ ∘ δ)
  π₂id[]      : π₂++ {Γ = Γ ++L Ω}{Δ = Γ}{Ω = Ω} id [ δ ]l ≡ π₂++ {Γ = Θ}{Δ = Γ}{Ω = Ω} δ
  ++l∘        : (_++l_ {Γ = Γ}{Δ = Δ}{Ω = Ω} γ ω) ∘ δ ≡
                _++l_ {Γ = Θ}{Δ = Δ}{Ω = Ω} (γ ∘ δ) (ω [ δ ]l)
  ++lβ₁       : π₁++ {Γ = Γ}{Δ = Δ}{Ω = Ω} (_++l_ {Ω = Ω} γ ω) ≡ γ
  {-# REWRITE ++lβ₁ #-}
  ++lβ₂       : π₂++ {Γ = Γ}{Δ = Δ}{Ω = Ω} (_++l_ {Ω = Ω} γ ω) ≡ ω
  ++lη        : _++l_ {Γ = Γ}{Δ = Δ}{Ω = Ω} (π₁++ {Γ = Γ}{Δ = Δ}{Ω = Ω} γ) (π₂++  {Γ = Γ}{Δ = Δ}{Ω = Ω} γ) ≡ γ
  {-# REWRITE ++lβ₂ π₂++[] ++l∘ ++lη π₂id[] #-}

id++l_ : Tms Γ Ω → Sub Γ (Γ ++L Ω)
id++l_ {Γ = Γ}{Ω = Ω} ω = _++l_ {Γ = Γ}{Δ = Γ}{Ω = Ω} id ω

postulate
  ++ε         : id ++l εL ≡ id {Γ = Γ}
  {-# REWRITE ++ε #-}
  
  -- extend a telescope with a type
  _▹L_        : (Ω : Tys Γ) → Ty (Γ ++L Ω) → Tys Γ
  _,l_        : (σ : Tms Γ Ω) → Tm Γ (A [ id++l σ ]T) → Tms Γ (Ω ▹L A)
  π₁l         : Tms Γ (Ω ▹L A) → Tms Γ Ω
  π₂l         : (σ : Tms Γ (Ω ▹L A)) → Tm Γ (A [ id++l π₁l σ ]T)
  -- , is a natural iso
  ++▹L        :  Γ ++L (Ω ▹L A) ≡ (Γ ++L Ω) ▹ A
  {-# REWRITE ++▹L #-}
  ++,         : id++l (ω ,l a) ≡ id++l ω ,s a

  _^^_        : (δ : Sub Γ Δ) → (Ω : Tys Δ) → Sub (Γ ++L Ω [ δ ]L) (Δ ++L Ω)
  ▹[]         : (Ω ▹L A) [ γ ]L ≡ (Ω [ γ ]L) ▹L (A [ γ ^^ Ω ]T)
  {-# REWRITE ▹[] #-}
  -- ,[]         : (ω ,l a) [ γ ]l ≡ (ω [ γ ]l) ,l (a [ γ ^^ Ω ]t)

  -- extend a tel with a tel
  _++LL_      : (Ω : Tys Γ) → Tys (Γ ++L Ω) → Tys Γ
  _++ll_      : (ω : Tms Γ Ω) → Tms Γ (Ψ [ id++l ω ]L) → Tms Γ (Ω ++LL Ψ)
  assL        : Γ ++L (Ω ++LL Ψ) ≡ (Γ ++L Ω) ++L Ψ
  {-# REWRITE assL #-}
  assLL       : (Ω ++LL Ψ) ++LL Ζ ≡ Ω ++LL (Ψ ++LL Ζ)
  {-# REWRITE assLL #-}
  idrLL       : Ω ++LL ◆L ≡ Ω
  {-# REWRITE idrLL #-}
  idlLL       : ◆L ++LL Ω ≡ Ω
  ++▹LL       : (Ω ++LL (Ψ ▹L A)) ≡ (Ω ++LL Ψ) ▹L A  -- do we need this?
  idrll       : {ω : Tms Γ Ω} → _++ll_ {Ψ = ◆L} ω εL ≡ ω
  idll        : ◆L ++LL Ω ≡ Ω

  Id          : (A : Ty Γ)(a b : Tm Γ A) → Ty Γ
  refll       : (a : Tm Γ A) → Tm Γ (Id A a a)
  Ids         : (Ω : Tys Γ)(ω ψ : Tms Γ Ω) → Tys Γ
  -- we follow Shulman by adding equations on types instead of
  -- definitional isomorphisms. we could add Ids◆ using a constructor,
  -- destructor etc.
  Ids◆        : Ids ◆L (εL {Γ = Γ}) εL ≡ ◆L
  Ids[]       : Ids Ω ω ψ [ γ ]L ≡ Ids (Ω [ γ ]L) (ω [ γ ]l) (ψ [ γ ]l)
  {-# REWRITE Ids[] #-}

p++ : Sub (Γ ++L Ω) Γ
p++ {Γ = Γ}{Ω = Ω} = π₁++ {Γ = Γ ++L Ω}{Δ = Γ}{Ω = Ω} id

q++ : Tms (Γ ++L Ω) (Ω [ p++ {Γ = Γ}{Ω = Ω} ]L)
q++ {Γ = Γ}{Ω = Ω} = π₂++ {Γ = Γ ++L Ω}{Ω = Ω} id

mid : Sub ((Γ ++L Ω) ++L Ψ [ p++ {Γ = Γ}{Ω = Ω} ]L) (Γ ++L Ψ)
mid {Γ = Γ}{Ω = Ω}{Ψ = Ψ} =
  _++l_
  {Γ = (Γ ++L Ω) ++L Ψ [ p++ {Γ = Γ}{Ω = Ω} ]L}
  {Δ = Γ}
  {Ω = Ψ}
  (p++ {Γ = Γ}{Ω = Ω} ∘ p++ {Γ = Γ ++L Ω}{Ω = Ψ [ p++ {Γ = Γ}{Ω = Ω} ]L})
  (q++ {Γ = Γ ++L Ω}{Ω = Ψ [ p++ {Γ = Γ}{Ω = Ω} ]L})

π₂id[]' : ∀{Γ Ω Θ δ} → q++ {Ω = Ω} [ δ ]l ≡ π₂++ {Γ = Θ}{Δ = Γ}{Ω = Ω} δ
π₂id[]' = refl

π₂id[]'' π₂id[]''' : {A : Ty (Γ ++L Ω)}{ω ψ : Tms Γ Ω}{a : Tm Γ (A [ id++l ω ]T)}{b : Tm Γ (A [ id++l ψ ]T)} → 
           (q++ {Ω = Ω [ p++ {Γ = Γ}{Ω = Ids Ω ω ψ} ]L} [ id ++l (ω [ p++ {Ω = Ids Ω ω ψ} ]l) ]l) ≡ (ω [ p++ {Ω = Ids Ω ω ψ} ]l)
π₂id[]'' {Γ = Γ}{Ω = Ω}{A = A}{ω}{ψ}{a}{b} = π₂id[]' {Ω = Ω [ p++ {Γ = Γ}{Ω = Ids Ω ω ψ} ]L}{δ = id ++l (ω [ p++ {Ω = Ids Ω ω ψ} ]l)}
π₂id[]''' = refl

postulate
  Idd         : (B : Ty (Γ ++L Ω))(ρ : Tms Γ (Ids Ω ω ψ)) →
                Tm Γ (B [ id++l ω ]T) → Tm Γ (B [ id++l ψ ]T) → Ty Γ
  Ids▹        : {A : Ty (Γ ++L Ω)}{ω ψ : Tms Γ Ω}
                {a : Tm Γ (A [ id++l ω ]T)}
                {b : Tm Γ (A [ id++l ψ ]T)}→
                Ids (Ω ▹L A) (ω ,l a) (ψ ,l b) ≡
                Ids Ω ω ψ ▹L {!Idd {Γ = Γ ++L Ids Ω ω ψ}{Ω = Ω [ p++ {Γ = Γ}{Ω = Ids Ω ω ψ} ]L}{ω = ω [ p++ ]l}{ψ = ψ [ p++ ]l}
                                    (A [ mid {Γ = Γ}{Ω = Ids Ω ω ψ}{Ψ = Ω} ]T) q++ !}

{-
                  Idd {Γ = Γ ++L Ids Ω ω ψ}
                      {Ω = Ω [ p++ {Γ = Γ}{Ω = Ids Ω ω ψ} ]L}
                      (A [ mid {Γ = Γ}{Ω = Ids Ω ω ψ}{Ψ = Ω} ]T)
                      (q++ {Γ = Γ}{Ω = Ids Ω ω ψ})
                      {!a [ p++ {Γ = Γ}{Ω = Ids Ω ω ψ} ]t!} -- (a [ p++ {Γ = Γ}{Ω = Ids Ω ω ψ} ]t)
                      {!!} -- (b [ p++ {Γ = Γ}{Ω = Ids Ω ω ψ} ]t)
-}
{-
mid ∘ ω [ p++ ] 
= (p++ ∘ p++ ++ q++ ) ∘ ω [ p++ ] 
= (p++ ∘ p++ ∘ ω [ p++ ] ++ q++ ∘ ω [ p++ ] )
         \_____________/
         (p++ ∘ (id ++ ω[p++])) = id
= (p++  ++ q++ ∘ (id ++ ω [ p++ ]) )                 (π₂++ id [ id ++l (ω [ π₁++ id ]l) ]l) =(π₂++[]) π₂++ (id ∘ id ++l (ω [ π₁++ id ]l)) = π₂++ (id ++l ω[p++]) =(π₂β) ω[p++]
= p++  ++    ω[p++]

(id∘p++ ++ ω[p++])
(id ++ ω) [ p++ ]
-}

infixl 5 _▹_
infixl 5 _▹L_
infixl 40 _[_]T
infixl 40 _[_]L
infixl 40 _[_∣_]T
infixl 40 _[id]T
infixl 5 _,s_
infixl 5 _,l_
infixr 8 _∘_
infixl 40 _[_]t
infixl 40 _[_]l
infixl 40 _[_∣_]t
