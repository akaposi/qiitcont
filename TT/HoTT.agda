{-# OPTIONS --rewriting  --type-in-type #-}

open import Relation.Binary.PropositionalEquality
open import Data.Product hiding (Σ)
open import Data.Unit

module TT.HoTT where

{-# BUILTIN REWRITE _≡_ #-}

Type = Set

postulate
  U : Type
  El : U → Type

variable
  A : U
  B : El A → U

postulate 

  Π : (A : U) → (El A → U) → U
  Π-eq : El (Π A B) ≡ ((a : El A) → El (B a))

  Σ : (A : U) → (El A → U) → U
  Σ-eq : El (Σ A B) ≡ (Σ[ a ∈ El A ] El (B a))
  
  Unit : U
  Unit-eq : El Unit ≡ ⊤

  UU : U
  U-eq : El UU ≡ U

{-# REWRITE Π-eq Σ-eq Unit-eq U-eq #-}

infixr 5 _⇒_

_⇒_ : U → U → U
A ⇒ B = Π A (λ _ → B)

variable
  A₀ A₁ : U
  B₀ : El A₀ → U
  B₁ : El A₁ → U
  a₀ : El A₀
  a₁ : El A₁

isEq : El (A₀ ⇒ A₁ ⇒ UU) → U
isEq = {!!}  

IdU : (A₀ A₁ : U) → U
IdU A₀ A₁ = Σ (A₀ ⇒ A₁ ⇒ UU) (isEq {A₀} {A₁})

Idd : (A₂ : El (IdU A₀ A₁)) → El A₀ → El A₁ → U
Idd (R , _) a₀ a₁ = R a₀ a₁

module _(A₂ : El (IdU A₀ A₁))
     {B₀ : El A₀ → U}{B₁ : El A₁ → U}
     (B₂ : {a₀ : El A₀}{a₁ : El A₁} → El (Idd {A₀} {A₁} A₂ a₀ a₁) → El (IdU (B₀ a₀) (B₁ a₁))) where

  ΣR : El (Σ A₀ B₀) → El (Σ A₁ B₁) → U
  ΣR (a₀ , b₀) (a₁ , b₁) = Σ (Idd {A₀} {A₁} A₂ a₀ a₁) (λ a₂ → Idd {B₀ a₀} {B₁ a₁} (B₂ a₂) b₀ b₁)

  Σ₂ : El (IdU (Σ A₀ B₀) (Σ A₁ B₁))
  Σ₂ = ΣR , {!!}

  ΠR : El (Π A₀ B₀) → El (Π A₁ B₁) → U
  ΠR f₀ f₁ = Π A₀ (λ a₀ → Π A₁ (λ a₁ →
                    Π (Idd {A₀} {A₁} A₂ a₀ a₁)
                        (λ a₂ → Idd {B₀ a₀} {B₁ a₁} (B₂ a₂) (f₀ a₀) (f₁ a₁))))

  Π₂ : El (IdU (Π A₀ B₀) (Π A₁ B₁))
  Π₂ = ΠR , {!!}

Unit₂ : El (IdU Unit Unit)
Unit₂ = (λ _ _ → Unit) , {!!}

UU₂ : El (IdU UU UU)
UU₂ = IdU , {!!}

Tel : Type
Tel = U

ε : Tel
ε = Unit

_,,_ : (Δ : Tel)(A : El Δ → U) → Tel
Δ ,, A = Σ Δ A

postulate
  IdTel : (Δ : Tel) → El Δ → El Δ → U

module _(Δ : Tel)(A : El Δ → U)
       {δ₀ δ₁ : El Δ}(δ₂ : El (IdTel Δ δ₀ δ₁)) where

  postulate

    apU : El (IdU (A δ₀) (A δ₁))

    ap : (f : (δ : El Δ) → El (A δ)) → El (Idd {A δ₀} {A δ₁} apU (f δ₀) (f δ₁))

module _(Δ : Tel)(A : El Δ → U)
       {δ₀ δ₁ : El Δ}(δ₂ : El (IdTel Δ δ₀ δ₁))
       (B : (δ : El Δ) → El (A δ) → U) where
       
  postulate
  
    apU-Σ-eq : apU Δ (λ δ → Σ (A δ) (B δ)) δ₂ ≡
      Σ₂ {A₀ = A δ₀} {A₁ = A δ₁} (apU Δ A δ₂)
         {B₀ = B δ₀}{B₁ = B δ₁} λ a₂ → apU (Δ ,, A) (λ (δ , a) → B δ a) {!!}   -- (δ₂ , a₂)
      
-- El (IdTel (Δ ,, A) (δ₀ , a₀) (δ₁ , a₁)) not yet defined!

       
