{-# OPTIONS --rewriting --without-K #-}
open import Relation.Binary.PropositionalEquality

module TT.Tel2 where

{-# BUILTIN REWRITE _≡_ #-}

infixl 5 _▹_
infixl 5 _▸_
infixl 40 _[_]T
infixl 40 _[_]t
infixl 40 _[_∣_]T
infixl 40 _[id]T
infixl 40 _[_∣_]t
infixl 40 _[id]t
infixl 5 _,s_
infixl 5 _,,_
infixl 5 _,,⟨_⟩_
infixr 8 _∘_
infixl 40 _[_]L
infixl 40 _[_]l
infixl 40 _[_∣_]L
infixl 40 _[id]L
infixl 40 _[_∣_]l
infixl 40 _[id]l

Type = Set

postulate
  Con : Type
  Sub : Con → Con → Type
  Ty  : Con → Type                   -- types
  Tm  : (Γ : Con) → Ty Γ → Type      -- terms
  Tys : Con → Type                   -- telescopes
  Tms : (Γ : Con) → Tys Γ → Type     -- telescopic terms

variable
  Γ Δ Θ Ξ : Con
  γ δ θ ϑ γa γω γωa : Sub Δ Γ
  A B C : Ty Γ
  a b c : Tm Γ A
  Ω Ψ Ζ : Tys Γ
  ω ψ ζ ωa : Tms Γ Ω   

postulate
  -- category
  _∘_         : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  ass         : (γ : Sub Δ Γ)(δ : Sub Θ Δ)(θ : Sub Ξ Θ) →
                (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
  id          : Sub Γ Γ
  idl         : {σ : Sub Δ Γ} → id ∘ σ ≡ σ
  idr         : {σ : Sub Δ Γ} → σ ∘ id ≡ σ
  {-# REWRITE ass idl idr #-}

  -- terminal object \diamond
  ⋄           : Con
  ε           : Sub Γ ⋄
  ⋄η          : γ ≡ ε

  -- family structure
  _[_]T       : Ty Γ → Sub Δ Γ → Ty Δ
  _[_∣_]T     : (A : Ty Γ)(γ : Sub Δ Γ)(δ : Sub Θ Δ) → A [ γ ]T [ δ ]T ≡ A [ γ ∘ δ ]T
  _[id]T      : (A : Ty Γ) → A [ id ]T ≡ A
  {-# REWRITE _[_∣_]T _[id]T #-}
  _[_]t       : Tm Γ A → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
  _[_∣_]t     : a [ γ ]t [ δ ]t ≡ a [ γ ∘ δ ]t
  _[id]t      : a [ id ]t ≡ a
  {-# REWRITE _[_∣_]t _[id]t #-}

  -- comprehension (Tm is locally representable) \t6
  _▹_         : (Γ : Con) → Ty Γ → Con
  _,s_        : (γ : Sub Δ Γ) → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
  π₁          :      Sub Δ (Γ ▹ A)  → Sub Δ Γ
  π₂          : (γa : Sub Δ (Γ ▹ A)) → Tm Δ (A [ π₁ γa ]T)
  ▹β₁         : π₁ (_,s_ {A = A} γ a) ≡ γ
  {-# REWRITE ▹β₁ #-}
  ▹β₂         : π₂ (_,s_ {A = A} γ a) ≡ a
  ▹η          : π₁ γa ,s π₂ γa ≡ γa
  π₁∘         : π₁ (γa ∘ δ) ≡ π₁ γa ∘ δ
  {-# REWRITE ▹β₂ ▹η π₁∘ #-}
  π₂[]        : π₂ γa [ δ ]t ≡ π₂ (γa ∘ δ)
  ,∘          : (γ ,s a) ∘ δ ≡ γ ∘ δ ,s a [ δ ]t
  {-# REWRITE π₂[] ,∘ #-}

_^_ : (γ : Sub Δ Γ)(A : Ty Γ) → Sub (Δ ▹ A [ γ ]T) (Γ ▹ A)
γ ^ A = γ ∘ π₁ id ,s π₂ id

postulate
  -- family structure
  _[_]L       : Tys Γ → Sub Δ Γ → Tys Δ
  _[_∣_]L     : (Ω : Tys Γ)(γ : Sub Δ Γ)(δ : Sub Θ Δ) → Ω [ γ ]L [ δ ]L ≡ Ω [ γ ∘ δ ]L
  _[id]L      : (Ω : Tys Γ) → Ω [ id ]L ≡ Ω
  {-# REWRITE _[_∣_]L _[id]L #-}
  _[_]l       : Tms Γ Ω → (γ : Sub Δ Γ) → Tms Δ (Ω [ γ ]L)
  _[_∣_]l     : ω [ γ ]l [ δ ]l ≡ ω [ γ ∘ δ ]l
  _[id]l      : ω [ id ]l ≡ ω
  {-# REWRITE _[_∣_]l _[id]l #-}

  -- comprehension (Tms is locally representable)
  _++_        : (Γ : Con) → Tys Γ → Con
  _,,_        : (γ : Sub Δ Γ) → Tms Δ (Ω [ γ ]L) → Sub Δ (Γ ++ Ω)
  ππ₁         :       Sub Δ (Γ ++ Ω)  → Sub Δ Γ
  ππ₂         : (γω : Sub Δ (Γ ++ Ω)) → Tms Δ (Ω [ ππ₁ γω ]L)
  ++β₁        : ππ₁ (_,,_ {Ω = Ω} γ ω) ≡ γ
  {-# REWRITE ++β₁ #-}
  ++β₂        : ππ₂ (_,,_ {Ω = Ω} γ ω) ≡ ω
  ++η         : ππ₁ γω ,, ππ₂ γω ≡ γω
  ππ₁∘        : ππ₁ (γω ∘ δ) ≡ ππ₁ γω ∘ δ
  {-# REWRITE ++β₂ ++η ππ₁∘ #-}
  ππ₂[]       : ππ₂ γω [ δ ]l ≡ ππ₂ (γω ∘ δ)
  ,,∘         : (γ ,, ω) ∘ δ ≡ γ ∘ δ ,, ω [ δ ]l
  {-# REWRITE ππ₂[] ,,∘ #-}

_^^_ : (γ : Sub Δ Γ)(Ω : Tys Γ) → Sub (Δ ++ Ω [ γ ]L) (Γ ++ Ω)
γ ^^ Ω = γ ∘ ππ₁ id ,, ππ₂ id

postulate
  -- empty telescope \dia
  ◆           : Tys Γ
  ◆[]         : ◆ [ γ ]L ≡ ◆
  ++◆         : Γ ++ ◆ ≡ Γ
  {-# REWRITE ◆[] ++◆ #-}
  ,,◆         : {γ : Sub Δ Γ} → (_,,_ {Ω = ◆} γ ω) ≡ γ
  {-# REWRITE ,,◆ #-}

εl : Tms Γ ◆
εl = ππ₂ {Ω = ◆} id
◆η : ω ≡ εl
◆η {Γ}{ω} = sym (++β₂ {Γ}{◆}{Γ}{id}{ω})

postulate
  -- extension of telescopes with a type \t5
  _▸_           : (Ω : Tys Γ) → Ty (Γ ++ Ω) → Tys Γ
  ▸[]           : (Ω ▸ A) [ γ ]L ≡ Ω [ γ ]L ▸ (A [ γ ^^ Ω ]T)
  ++▸           : Γ ++ (Ω ▸ A) ≡ (Γ ++ Ω) ▹ A
  {-# REWRITE ▸[] ++▸ #-}
--  ππ▸           : ππ₁ {Ω = Ω ▸ A} γωa ≡ ππ₁ {Ω = Ω} (π₁ γωa)
--  {-# REWRITE ππ▸ #-}
  -- these can be derived from ππ▸
  ππ▸₁          : ππ₁ {Ω = Ω ▸ A} ((_,,_ {Ω = Ω} γ ω) ,s a) ≡ γ
  ππ▸₂          : ππ₁ {Ω = Ω} (π₁ {A = A} (_,,_ {Ω = Ω ▸ A} γ ωa)) ≡ γ
  {-# REWRITE ππ▸₁ ππ▸₂ #-}

_,l_ : (ω : Tms Γ Ω) → Tm Γ (A [ id ,, ω ]T) → Tms Γ (Ω ▸ A)
_,l_ {Ω = Ω}{A = A} ω a = ππ₂ {Ω = Ω ▸ A} (id ,, ω ,s a)

π₁l : Tms Γ (Ω ▸ A) → Tms Γ Ω
π₁l {Ω = Ω} ωa = ππ₂ {Ω = Ω} (π₁ (id ,, ωa))

-- π₁ (id ,, ωa) ≡ id ,, ππ₂ (π₁ (id ,, ωa))
ee : π₁ {A = A} (_,,_ {Ω = Ω ▸ A} id ωa) ≡ _,,_ {Ω = Ω} (id {Γ}) (ππ₂ {Ω = Ω} (π₁ (id ,, ωa)))
ee {Γ = Γ}{Ω = Ω}{ωa = ωa} = sym (++η {Γ}{Γ}{Ω}{π₁ (id ,, ωa)})

π₂l : (ωa : Tms Γ (Ω ▸ A)) → Tm Γ (A [ id ,, π₁l ωa ]T)
π₂l {Γ = Γ}{Ω = Ω}{A = A} ωa = subst
  (λ γω → Tm Γ (A [ γω ]T))
  ee
  (π₂ (id ,, ωa))

--    ππ₂ (π₁ (id ,, ππ₂ (id ,, ω ,s a))) ≡ ω
▸β₁ : π₁l (_,l_ {Ω = Ω}{A = A} ω a) ≡ ω
▸β₁ {Ω = Ω}{A = A}{ω = ω}{a = a} = {! (++η {γω = _,s_ {A = A} (_,,_ {Ω = Ω} id ω) a})!}
