\documentclass{article}

\title{The Universe}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathpartir}

\newcommand{\type}{\ \mathbf{type}}
\newcommand{\Id}[3]{\mathbf{Id}_{#1}(#2,#3)}
\newcommand{\Idd}[4]{\mathbf{Id}_{#1}^{#2}(#3,#4)}
\newcommand{\refl}[1]{\mathbf{refl}_{#1}}
\newcommand{\HH}{\mathbf{H}}
\newcommand{\hdstr}{\mathbf{hdstr}}
\newcommand{\hcstr}{\mathbf{hcstr}}
\newcommand{\ap}[2]{\mathbf{ap}_{#1}(#2)}
\newcommand{\UU}{\mathbf{U}}
\newcommand{\El}{\mathbf{El}}
\newcommand{\isEquiv}{\mathbf{isEquiv}}
\newcommand{\isProp}{\mathbf{isProp}}
\newcommand{\RR}{\mathrm{R}}
\newcommand{\coe}{\mathrm{coe}}
\newcommand{\coh}{\mathrm{coh}}
\newcommand{\prp}{\mathrm{prp}}
\newcommand{\ot}{\leftarrow}
\newcommand{\sigmap}{\hat{\sigma}}
\newcommand{\pip}{\hat{\pi}}

\begin{document}
\maketitle

I am presenting an alternative to Mike's universe construction which
avoids the use of recursive higher coinductive types but only uses a
non-recursive case which I call higher dependent types.

The definition of the universe equality is basically Voevodsky's
definition of a one-to-one correspondence as described in section
14. However, we are using $\isProp(A)$ for $A : \UU$ which is defined
using a higher dependent type.

Given $A_0,A_1$ the type $\Id{\UU}{A_0}{A_1}$ is given by the following
$\Sigma$-type :
\begin{align*}
  \RR & : A_0 \to A_1 \to \UU \\
  \coe^\to &  : A_0 \to A_1 \\
  \coh^\to & : \Pi(a_0 : A_0) \RR(a_0,\coe^\to(a_0)) \\
  \prp^\to & : \Pi(a_0 : A_0) \isProp(\Sigma(a_1 : A_1))
             \RR(a_0,a_1))\\
  \coe^\ot &  : A_1 \ot A_0 \\
  \coh^\ot & : \Pi(a_1 : A_1) \RR(\coe^\ot(a_1),a_1) \\
  \prp^\ot & : \Pi(a_1 : A_1) \isProp(\Sigma(a_0 : A_0) \RR(a_0,a_1))\\  
\end{align*}

The definition is nicely symmetric which also entails that symmetry is
trivial. It expresses that $\RR$ is functional in both
directions. Indeed we can rewrite this definition as
\[
\Id{\UU}{A_0}{A_1} = \Sigma (R : A_0 \to A_1 \to \UU) \isEquiv(R)
\]
We exploit the non-fibrant view of the universe in our definition
below which is then used to formulate fibrancy.

To be able to define $\Pi$ and $\Sigma$-types in the universe we need
to establish that they are congruences (so that we can define
$\mathbf{ap}$). That is we need to show
\[
  \inferrule{
    \Gamma \vdash A_0,A_1 : \UU \\
    \Gamma \vdash A_2 : \Id{\UU}{A_0}{A_1}\\\\
    \Gamma \vdash B_0 : A_0 \to \UU \\
    \Gamma \vdash B_1 : A_1 \to \UU \\\\
    \Gamma \vdash B_2 : \Pi(a_0 : A_0)(a_1 : A_1)(a_2 : \Idd{\UU.\El}{A_2}{a_0}{a_1})
    \Id{\UU}{B_0(a_0)}{B_1(a_1)}}
  {\Gamma \vdash \sigma(A_2,B_2) :
    \Id{\UU}{\Sigma(A_0,B_0)}{\Sigma(A_1,B_1)}\\
    \Gamma \vdash \pi(A_2,B_2) :
    \Id{\UU}{\Pi(A_0,B_0)}{\Pi(A_1,B_1)}}
\]

The construction for $\Sigma$-types is the same as Mike's for
$\RR,\coe,\coh$ and for $\Pi$ also $\coh$ is different because it
relies on $\prp$.  

It turns out that constructing $\sigma$ and $\pi$ relies on the
following constructions for $\isProp$:
% \[
% A : U, \alpha : \isProp(A), B : A \to U, \beta : \Pi(a : A) \to
% \isProp(B(a))
% \vdash \sigmap(\alpha,\beta) : \isProp(\Sigma(A,B)
% \]

\[
  \inferrule
  { \Gamma \vdash A : U \\
    \Gamma \vdash \alpha : \isProp(A)\\\\
    \Gamma \vdash B : A \to U \\
    \Gamma \vdash \beta : \Pi(a : A) \to \isProp(B(a))}
  {\Gamma \vdash \sigmap(\alpha,\beta) : \isProp(\Sigma(A,B))}
\]
% \[
% A : U, B : A \to U, \beta : \Pi(a : A) \to
% \isProp(B(a))
% \vdash \sigmap(\beta) : \isProp(\Sigma(A,B)
% \]
\[
  \inferrule
  { \Gamma \vdash A : U \\
    \Gamma \vdash B : A \to U \\
    \Gamma \vdash \beta : \Pi(a : A) \to \isProp(B(a))}
  {\Gamma \vdash \pip(\beta) : \isProp(\Pi(A,B))}
  \]
These properties hold not only intuitively but they can be shown in
HoTT using path induction. However, we cannot exploit path induction
yet, because this is a consequence of our construction.

On the other hand the properties cannot be directly verified using the
\emph{obvious} definition of $\isProp$:
\[ \isProp(A) = \Pi(a_0, a_1:A)\Id{A}{a_0}{a_1} \quad ?\]
We need a stronger invariant for the assumption $\beta$ in both cases,
namely
\[ \beta^* : \Pi(a_0,a_1 : A)(a_2 : \Id{A}{a_0}{a_1})(b_0 :
  B(a_0))(b_1 : B(a_1)) \Idd {\UU.\El}{\ap{B}{a_2}}{b_0}{b_1}
  \] 
This suggests an alternative definition of $\isProp$ using a
non-recursive higher coinductive type, which I am calling \emph{higher
  dependent type}, that is given by the following higher destructor:
\[
  \inferrule{
    A_0, A_1 : U \\ A_2 : \Id{U}{A_0}{A_1} \\\\
    \alpha_0 : \isProp(A_0) \\ \alpha_1 : \isProp(A_1) \\
    \alpha_2 : \Idd{A:U.\isProp(A)}{A_2}{\alpha_0}{\alpha_1}}
  {\hdstr(\alpha_2) : \Pi(a_0:A_0)(a_1 : A_1) \Idd {\UU.\El}{A_2}{a_0}{a_1}}   
\]
And as in Mike's draft we can show that an instance of the higher
destructor implies the usual definition of $\isProp$:
\[
  \inferrule{A : U \\\alpha : \isProp(A)}
  {\hdstr(\refl{\alpha}) : \Pi(a_0, a_1:A)\Id{A}{a_0}{a_1}}
\]
Using the higher version of $\isProp$ we can show the closure under
$\sigmap$ and $\pip$ using the closed co-eliminator and the destructor. The idea is
that once we move to the view of $\isProp$ as a higher dependent type
we can establish the closure. Hence derive the operations $\sigma$ and
$\pi$ and complete the construction of the universe.

It is interesting to note that the presence of higher dependent types
corresponds to having a right adjoint to the path functor. This means
we can only interpret the syntax in uniform cubical sets but not in
uniform simplicial sets. It remains to investigate wether the universe
constructed from the syntax is the same as the known universe for
uniform cubical sets.
   
\section*{Higher dependent types}
\label{sec:high-depend-types}

This is just the non-dependent case of the higher coinductive types
from Mike's draft (section 10).  They correspond to having the right
adjoint to the path type semantically.

\[
  \inferrule{\vdash I  \type\\\\
    i_0 : I, i_1 : I, i_2 :  \Id{I}{i_0}{i_1} \vdash C \type \\\\
    \Gamma \vdash i : I}
  {\Gamma \vdash \HH(I,C,i)\type}
\]
  
\[
  \inferrule{\Gamma \vdash
    \delta_2 : \Id{\Delta}{\delta_0}{\delta_1} \\
   \Gamma \vdash e_2 : \Idd{\Delta.\HH(I,C,i)}{\delta_2}{e_0}{e_1}}
  {\Gamma \vdash \hdstr(e_2) :
    C[i[\delta_0],i[\delta_1],\ap{\Delta.i}{\delta_2}]}
\]

\[
  \inferrule{\vdash K \type \\ k : K \vdash r : I \\\\
  k_0 : K, k_1 : K, k_2 :  \Id{K}{k_0}{k_1}
  \vdash m : C[r[k_0],r[k_1],\ap{r}{k_2}]\\\\
  \Gamma \vdash k : K}
{\Gamma\vdash \hcstr(K,r,m,k) : \HH(I,C,r[k])}
\]

\[
  \inferrule{\Gamma \vdash
    \delta_2 : \Id{\Delta}{\delta_0}{\delta_1} \\\\
    \vdash K \type \\ k : K \vdash r : I \\\\
  k_0 : K, k_1 : K, k_2 :  \Id{K}{k_0}{k_1}
  \vdash m : C[r[k_0],r[k_1],\ap{r}{k_2}]}
{\Gamma\vdash \hdstr(\ap{\delta_2}{\Delta.\hcstr(K,r,m,k)}) \equiv m }
\]

We define
\[ \isProp(A) = \HH(\UU,C_\isProp,A) \]
where
\[ A_0, A_1 : U,A_2 : \Id{U}{A_0}{A_1} \vdash
  C_\isProp = \Pi(a_0:A_0)(a_1 : A_1) \Idd{A_2}{\UU.\El}{a_0}{a_1} \]
We obtain
\[
  \inferrule{
    A_0, A_1 : U \\ A_2 : \Id{U}{A_0}{A_1} \\\\
    \alpha_0 : \isProp(A_0) \\ \alpha_1 : \isProp(A_1) \\
    \alpha_2 : \Idd{A_2}{A.\isProp(A)}{\alpha_0}{\alpha_1}}
  {\hdstr(\alpha_2) : \Pi(a_0:A_0)(a_1 : A_1) \Idd{A_2}{\UU.\El}{a_0}{a_1}}   
\]

We can use $\hcstr$ to derive $\sigmap$ and $\pip$.

\subsection*{$\sigmap$}

We first consider
$\sigmap$, we define $K$ as a $\Sigma$-type 
\[ K = \Sigma (A : U)(\alpha : \isProp(A))(B : A \to U)(\beta : \Pi(a : A) \to
  \isProp(B(a)))\]
We define $K \vdash r : U$ as:
\[ r(A,\alpha,B,\beta) = \Sigma(A,B) \]
We need to define the \emph{method} $m$ in the following context
after flattening $\Sigma$-types and reordering assumptions:
\[
  \inferrule
  { A_0, A_1 : U \\  A_2 : \Id{U}{A_0}{A_1} \\\\
  \alpha_0 : \isProp(A_0) \\ \alpha_1 : \isProp(A_1) \\\\
  \alpha_2 : \Idd{\ap{\isProp}(A_2)}{A.\isProp(A)}{\alpha_0}{\alpha_1}\\\\
  B_0 : A_0 \to \UU \\ B_1 : A_1 \to \UU \\\\
  B_2 :  \Pi(a_0:A_0)(a_1:A_1)(a_2 : \Idd{A_2}{\UU.\El}{a_0}{a_1}) \Id{U}{B_0(a_0)}{B_1(a_1)}\\\\
  \beta_0 : \Pi(a_0 : A_0) \isProp(B_0(a_0)) \\
  \beta_1 : \Pi(a_1 : A_1) \isProp(B_1(a_1)) \\\\
  \beta_2 : \Pi(a_0:A_0)(a_1:A_1)(a_2 : \Idd{A_2}{\UU.\El}{a_0}{a_1})
  \Idd{\ap{\isProp}{B_2(a_2)}}{}{\beta_0(a_0)}{\beta_1(a_1)}
}
{
  m : \Pi (p_0 : \Sigma(A_0,B_0))(p_1 : \Sigma (A_1,B_1))
    \Idd{\ap{\Sigma}{A_2,B_2}}{}{p_0}{p_1}
  }
\]
Note that we are using the behaviour of $\Sigma$ on the non-fibrant
equality in the universe in the conclusion, ie.e the definition of
$\RR$ which is forced upon us by the equality of $\Sigma$-types.

We can expand the domain of $m$ as
\begin{align*}
  a_0 & : A_0 \\
  b_0 & : B_0 (a_0) \\
  a_1 & : A_1 \\
  b_0 & : B_0 (a_0) 
\end{align*}
and the codomain :
\begin{align*}
  a_2 & : \Idd{A_2}{}{a_0}{a_1} \\
  b_2 & : \Idd{B_2(a_2)}{}{b_0}{b_1}
\end{align*}
Using $\hdstr$ we can derive
\begin{align*}
  a_2 = \hdstr(\alpha_2)(a_0,a_1) \\
  b_2 = \hdstr(\beta_2(a_0,a_1,a_2))(b_0,b_1)
\end{align*}

\subsection*{$\pip$}

The construction of $\pip$ is very similar, we define $K$ as a $\Sigma$-type 
\[ K = \Sigma (A : U)(B : A \to U)(\beta : \Pi(a : A) \to
  \isProp(B(a)))\]
We define $K \vdash r : U$ as:
\[ r(A,B,\beta) = \Pi(A,B) \]
We need to define the \emph{method} $m$ in the following context
after flattening $\Sigma$-types and reordering assumptions:
\[
  \inferrule
  { A_0, A_1 : U \\  A_2 : \Id{U}{A_0}{A_1} \\\\
  B_0 : A_0 \to \UU \\ B_1 : A_1 \to \UU \\\\
  B_2 :  \Pi(a_0:A_0)(a_1:A_1)(a_2 : \Idd{A_2}{\UU.\El}{a_0}{a_1}) \Id{U}{B_0(a_0)}{B_1(a_1)}\\\\
  \beta_0 : \Pi(a_0 : A_0) \isProp(B_0(a_0)) \\
  \beta_1 : \Pi(a_1 : A_1) \isProp(B_1(a_1)) \\\\
  \beta_2 : \Pi(a_0:A_0)(a_1:A_1)(a_2 : \Idd{A_2}{\UU.\El}{a_0}{a_1})
  \Idd{\ap{\isProp}{B_2(a_2)}}{}{\beta_0(a_0)}{\beta_1(a_1)}
}
{
  m : \Pi (f_0 : \Pi(A_0,B_0))(f_1 : \Pi (A_1,B_1))
    \Idd{\ap{\Pi}{A_2,B_2}}{}{f_0}{f_1}
  }
\]
This time there is no need to massage the domain, but the codomain
corresponds to 
\[
  m(f_0,f_1) :
  \Pi(a_0:A_0)(a_1:A_1)(a_2 : \Idd{A_2}{\UU.\El}{a_0}{a_1})
  \Idd{\ap{\isProp}{B_2(a_2)}}{}{f_0(a_0)}{f_1(a_1)}
\]
which can be obtained using $\hdstr$:
\[
  m(f_0,f_1) = \hdstr(\beta_2(a_0,a_1,a_2))(f_0(a_0),f_1(a_1))
\]
 
\section*{Closure under $\Sigma$}

We are now ready to show that the universe is closed under $\Sigma$,
i.e. 
\[
  \inferrule{
    \Gamma \vdash A_0,A_1 : \UU \\
    \Gamma \vdash A_2 : \Id{\UU}{A_0}{A_1}\\\\
    \Gamma \vdash B_0 : A_0 \to \UU \\
    \Gamma \vdash B_1 : A_1 \to \UU \\\\ 
    \Gamma \vdash B_2 : \Pi(a_0 : A_0)(a_1 : A_1)(a_2 : \Idd{\UU.\El}{A_2}{a_0}{a_1})
    \Id{\UU}{B_0(a_0)}{B_1(a_1)}}
  {\Gamma \vdash \sigma(A_2,B_2) :
    \Id{\UU}{\Sigma(A_0,B_0)}{\Sigma(A_1,B_1)}}
\]

As mentioned above $\RR,\coe,\coh$ are basically as in Mike's draft.
\begin{align*}
  & \RR : \Sigma(A_0,B_0) \to \Sigma(A_1,B_1) \to \UU \\
  & \RR (a_0 , b_0) (a_1 , b_1) =
    \Sigma (r : A_2.\RR  (a_0 , a_1)) (B_2(r)).\RR (b_0,b_1) \\
  & \coe^\to : \Sigma(A_0,B_0) \to \Sigma(A_1,B_1) \\
  & \coe^\to (a_0,b_0) = (A_2.\coe^\to(a_0),(B_2
    (A_2.\coh^\to(a_0))).\coe^\to(b_0))\\
  & \coh^\to : \Pi (x_0 : \Sigma(A_0,B_0)) \RR (x_0, coe\to(x_0)) \\
  & \coh^\to (a_0 , b_0) = (A_2.\coh^\to(a_0),(B_2
    (A_2.\coh^\to(a_0))).\coh^\to(b_0))
\end{align*}

It remains to derive
\begin{align*}
  & \prp^\to : \Pi(x_0 : \Sigma(A_0,B_0)) \isProp(\Sigma(x_1 : \Sigma(A_1,B_1)))
             \RR(x_0,x_1)
\end{align*}
Assume $x_0 = (a_0 : A_0, b_0 : B_0 a_0)$ We need to do show
\[ \isProp(\Sigma ((a_1,b_1): \Sigma(A_1,B_1)) . \RR ((a_0,b_0)
  (a_1,b_1))) \]
which can be expanded and rewritten as:
\[ \isProp(\Sigma (a_1 : A_1)(b_1 : B_1(a_1))(a_2 : A_2.\RR(a_0,a_1))
  (b_2 : B_2(a_2).\RR(b_0,b_1))\]
We can rearrange this again by separating the $A$ and $B$ components
exploiting associativity of $\Sigma$ :
\[ \isProp(\Sigma (a_1,a_2) : \Sigma (a_1 : A_1) (a_2 :
  A_2.\RR(a_0,a_1)) \Sigma (b_1 : B_1(a_1))
  (b_2 : B_2(a_2).\RR(b_0,b_1)))\]
and now applying $\sigmap$ this reduces to:
\begin{align*}
  &\isProp(\Sigma (a_1 : A_1) (a_2 :
  A_2.\RR(a_0,a_1))\\
  &\Pi (a_1 : A_1)(a_2 : \RR(a_0,a_1)) \isProp(\Sigma (b_1 : B_1(a_1))
  (b_2 : B_2(a_2).\RR(b_0,b_1)))
\end{align*}
The two components are given by
\begin{align*}
  & A_2.\prp^\to (a_0) \\
  & \lambda (a_1,a_2) . B_2(a_2).\prp^\to(b_0)
\end{align*}
The components $\coe^\ot,\coh^\ot,\prp^\ot$ can be constructed
symmetrically.

\section*{Closure under $\Pi$}

As usual the closure under $\Pi$ is a bit more involved and also mixes
the conditions for the different directions.

\[
  \inferrule{
    \Gamma \vdash A_0,A_1 : \UU \\
    \Gamma \vdash A_2 : \Id{\UU}{A_0}{A_1}\\\\
    \Gamma \vdash B_0 : A_0 \to \UU \\
    \Gamma \vdash B_1 : A_1 \to \UU \\\\ 
    \Gamma \vdash B_2 : \Pi(a_0 : A_0)(a_1 : A_1)(a_2 : \Idd{\UU.\El}{A_2}{a_0}{a_1})
    \Id{\UU}{B_0(a_0)}{B_1(a_1)}}
 {\Gamma \vdash \pi(A_2,B_2) :
    \Id{\UU}{\Pi(A_0,B_0)}{\Pi(A_1,B_1)}}
\]

This time $\RR,\coe$ are as in Mike's draft.
\begin{align*}
  & \RR : \Pi(A_0,B_0) \to \Pi(A_1,B_1) \to \UU \\
  & \RR (f_0,f_1) =
    \Pi (a_0 : A_0)(a_1 : A_1)(a_2 : A_2.\RR(a_0,a_1)) B_2(a_2).\RR(f_0(a_0),f_1(a_1))\\
  & \coe^\to : \Pi(A_0,B_0) \to \Pi(A_1,B_1) \\
  & \coe^\to(f_0) =\lambda (a_1 : A_1) .B_2(A_2.\coh^\ot(a_1)).\coe^\to(f_0(A_2.\coe^\ot(a_1)))
\end{align*}

$\coh$ depends on $\prp$ hence it is different. We need to show

\begin{align*}
   \coh^\to & : \Pi(f_0 : \Pi(A_0,B_0)) \RR(f_0,\coe^\to(f_0))
\end{align*}
this expands to
\begin{align*}
  \coh^\to & : \Pi(f_0 : \Pi(A_0,B_0))(a_0 : A_0)(a_1 : A_1)(a_2 :
             A_2.\RR(a_0,a_1))\\
  & \quad B_2(a_2).\RR(f_0(a_0),
             B_2(A_2.\coh^\ot(a_1)).\coe^\to(f_0(A_2.\coe^\ot(a_1))))
\end{align*}

We assume as given $f_0,a_0,a_1,a_2$. We know
\begin{align*}
  a_2 & : A_2.\RR(a_0,a_1) \\
  \coh^\ot(a_1)) : &  \RR(A_2.\coe^\ot(a_1),a_1) \\
\end{align*}
Hence using the simple destructor on $A_2.\prp^\ot(a_1)$ we know that
\[ \Id{\Sigma(a_0:A_0)(A_2.\RR(a_0,a_1))}{(a_0,a_2}{A_2.\coe^\ot(a_1),
    \coh^\ot(a_1)} \]
which can be destructed into
\begin{align*}
  & q : \Id{A_0}{a_0}{\coe^\ot(a_1)}\\
  & r : \Idd{}{q}{a_2}{\coh^\ot(a_1)}
\end{align*}
Now we know
\[
  q' : \ap{f_0}{q} : \Idd{}{q}{f_0(a_0)}{f_0(A_2.\coe^\ot(a_1))}
\]
We can also show
\[
  B_2(\coe^\ot(a_1).\RR(f_0(A_2.\coe^\ot(a_1),\coe^\to(f_0(A_2.\coe^\ot(a_1)))))
\]
using $B_2(\coe^\ot(a_1)).\coh^\to$. Now using $r$ and $q'$ we know
that in $U$ this is equal to our goal
\[
B_2(a_2).\RR(f_0(a_0),
B_2(A_2.\coh^\ot(a_1)).\coe^\to(f_0(A_2.\coe^\ot(a_1))))
\]
At this point we appeal to $\coe$ for this equality. Clearly, this is
a crucial step where we assume that the relation in the input already
refers to a fibrant universe.







 







\end{document}
